﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ReiseBuro.Repository.IRepository;
using ReiseBuro.Repository.Repository;
using ReiseBuro.Entities.Models;
using System.Collections.Generic;
using System.Linq;
using Itenso.TimePeriod;

namespace ReiseBuro.UnitTest
{
    [TestClass]
    public class UnitTest1
    {

        public ICurrencyRepository _currep = new CurrencyRepository();
        public IUserRepository _userrep = new UserRepository();
        public IUserActionRepository _useractionrep = new UserActionRepository();
        private readonly ICampaignRepository _campaignRepository = new CampaignRepository();



        [TestMethod]
        public void TestDateRange()
        {
            //List<TimeRange> timeRange = new List<TimeRange>();
            //List<TimeBlock> timeBlock = new List<TimeBlock>();
            //List<TimeInterval> timeInterval=new List<TimeInterval>();


            var camps = _campaignRepository.GetAll().Where(a => a.BookingBegin <= DateTime.Now && a.BookingEnd >= DateTime.Now && a.IsActive == true).ToList();
            Dictionary<int, TimeInterval> campaignDict = new Dictionary<int, TimeInterval>();
            Dictionary<int, TimeBlock> campaignBlock = new Dictionary<int, TimeBlock>();

            foreach (var item in camps)
            {
                campaignDict.Add(item.Id, new TimeInterval(item.CheckIn, item.CheckOut));
                campaignBlock.Add(item.Id, new TimeBlock(item.CheckIn, item.CheckOut));
            }

            var countObj = new List<Campaign>();


            while (camps.Count > 0)
            {
              
                for (var j = 0; j < camps.Count; j++)
                {                    
                    for (int i = camps.Count; i > j; i--)
                    {
                        if (campaignDict[camps[i-1].Id].IntersectsWith(campaignDict[camps[i - 2].Id]))
                        {
                            var relation = campaignDict[camps[i-2].Id].GetRelation(campaignDict[camps[i -2].Id]);
                            switch (relation)
                            {
                                case PeriodRelation.After:

                                    break;
                                case PeriodRelation.StartTouching:

                                    break;
                                case PeriodRelation.StartInside:
                                    break;

                                case PeriodRelation.InsideStartTouching:
                                    break;

                                case PeriodRelation.EnclosingStartTouching:
                                    break;

                                case PeriodRelation.Enclosing:
                                    break;

                                case PeriodRelation.EnclosingEndTouching:
                                    break;

                                case PeriodRelation.ExactMatch:
                                    countObj.Add(camps[i-1]);
                                    camps.Remove(camps[i - 2]);
                                    break;

                                case PeriodRelation.Inside:
                                    break;

                                case PeriodRelation.InsideEndTouching:

                                    break;

                                case PeriodRelation.EndInside:
                                    break;

                                case PeriodRelation.EndTouching:
                                    break;

                                case PeriodRelation.Before:
                                    break;

                                default:
                                    var intersect = campaignDict[camps[i-1].Id].GetIntersection(campaignDict[camps[i -2].Id]);

                                    break;
                            }


                        }
                    }

                }
            }


        }
    }
}
