﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReiseBuro.Entities.Helper
{
    public static class SetLanguageVal
    {
        public static IEnumerable<Models.Dictionary> AllDictionary { get; set; }
        public static string cultureString { get; set; } = "tr";

        public static string GetContent(string keyword)
        {
            if (String.IsNullOrEmpty(keyword))
                return "";
            var returnStr = "";
            returnStr= AllDictionary.FirstOrDefault(f => f.D_Culture == cultureString && f.D_Key == keyword).D_Value;
            return returnStr;
        }

    }
}
