﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReiseBuro.Entities.Models
{
    [Table("Log_Exception")]
    public class Log_Exception
    {
        [Key]
        public int Id { get; set; }
        public DateTime ErrorDate { get; set; }
        public string ExceptionType { get; set; }
        public string FileName { get; set; }
        public int LineNumber { get; set; }
        public string MethodName { get; set; }
        public string ClassName { get; set; }
        public string StackTrace { get; set; }
        public string ErrorMessage { get; set; }
        public string InnerException { get; set; }
        public string InnerExceptionMessage { get; set; }
    
    }
}
