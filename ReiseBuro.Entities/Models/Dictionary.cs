﻿using Dapper.Contrib.Extensions;
using ReiseBuro.Entities.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReiseBuro.Entities.Models
{
    [Table("Dictionary")]
    public class Dictionary
    {
        [Dapper.Contrib.Extensions.Key]
        public int Id { get; set; }

        [Display(Name = "key", ResourceType = typeof(Resource))]
        public string D_Key { get; set; }

        [Display(Name = "value", ResourceType = typeof(Resource))]
        public string D_Value { get; set; }

        [Display(Name = "dil", ResourceType = typeof(Resource))]
        public string D_Culture { get; set; }

    }
}
