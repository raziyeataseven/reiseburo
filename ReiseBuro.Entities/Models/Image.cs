﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReiseBuro.Entities.Models
{
    [Table("Image")]
    public class Image
    {
        [Key]
        public int Id { get; set; }
        public string Culture { get; set; }
        public string ImagePath { get; set; }
        public string ImageCaption { get; set; }
        public string ImageDescription { get; set; }
        public int? ImageOrder { get; set; }

    }
}
