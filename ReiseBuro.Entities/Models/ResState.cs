﻿using Dapper.Contrib.Extensions;
using ReiseBuro.Entities.Resources;
using System.ComponentModel.DataAnnotations;

namespace ReiseBuro.Entities.Models
{
    [Table("ResState")]
    public class ResState
    {
        [Dapper.Contrib.Extensions.Key]
        public int Id { get; set; }

        [Required]
        [Display(Name = "durum", ResourceType = typeof(Resource))]
        public string ConfirmState{ get; set; }

        [Required]
        [Display(Name = "kod", ResourceType = typeof(Resource))]
        public string ConfirmCode { get; set; }

        [Display(Name = "sozluktekikarsilik", ResourceType = typeof(Resource))]
        public string ActionKeyword { get; set; }

        public string DisplayName { get; set; }//{ get { return Helper.SetLanguageVal.GetContent(this.ActionKeyword); } set { _displayName = value; } }

        private string _displayName;

        public ResState()
        {
            if (this.DisplayName != null)
                ConfirmState = DisplayName;

        }
    }
}
