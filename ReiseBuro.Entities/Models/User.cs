﻿using Dapper.Contrib.Extensions;
using ReiseBuro.Entities.Resources;
using System;
using System.ComponentModel.DataAnnotations;

namespace ReiseBuro.Entities.Models
{
    [Table("Users")]
    public class User
    {
        [Dapper.Contrib.Extensions.Key]
        public int Id { get; set; }

        [Display(Name = "bounduser", ResourceType = typeof(Resource))]
        public int? BoundUser { get; set; }

        [Display(Name = "isim", ResourceType = typeof(Resource))]
        public string Name { get; set; }

        [Display(Name = "soyisim", ResourceType = typeof(Resource))]
        public string Surname { get; set; }

        public string Email { get; set; }

        [Display(Name = "kullanici", ResourceType = typeof(Resource))]
        public string UserName { get; set; }//username email adresiyle aynı olacak

        [Display(Name = "parola", ResourceType = typeof(Resource))]
        public string Password { get; set; }

        [Display(Name = "aktifmi", ResourceType = typeof(Resource))]
        public bool IsActive { get; set; }

        [Display(Name = "rol", ResourceType = typeof(Resource))]
        public string Role { get; set; }

        [Display(Name = "yetkili", ResourceType = typeof(Resource))]
        public string PersonInCharge { get; set; }

        [Display(Name = "adres", ResourceType = typeof(Resource))]
        public string UserAddress { get; set; }

        [Display(Name = "ulke", ResourceType = typeof(Resource))]
        public string CountryCode { get; set; }

        [Display(Name = "sehir", ResourceType = typeof(Resource))]
        public string City { get; set; }

        [Display(Name = "kayittarih", ResourceType = typeof(Resource))]
        public DateTime? RegistryDate { get; set; }

        [Display(Name = "onaytarih", ResourceType = typeof(Resource))]
        public DateTime? ConfirmDate { get; set; }

        [Display(Name = "telefon", ResourceType = typeof(Resource))]
        public string Gsm { get; set; }

        [Display(Name = "telefon", ResourceType = typeof(Resource))]
        public string Telephone { get; set; }
    }
}
