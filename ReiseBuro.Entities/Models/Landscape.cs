﻿using Dapper.Contrib.Extensions;
using System.ComponentModel.DataAnnotations;

namespace ReiseBuro.Entities.Models
{
    [Table("Landscape")]
    public class Landscape
    {
        [Dapper.Contrib.Extensions.Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
    }
}
