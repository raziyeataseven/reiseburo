﻿using Dapper.Contrib.Extensions;
using ReiseBuro.Entities.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReiseBuro.Entities.Models
{
    [Table("Reservation")]
    public class Reservation
    {
        [Dapper.Contrib.Extensions.Key]
        public int Id { get; set; }

        [Display(Name = "kullanici", ResourceType = typeof(Resource))]
        public int UserId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
            ErrorMessageResourceName = "zorunluAlan")]
        [Display(Name = "voucherno", ResourceType =typeof(Resource))]
        public string VoucherNo { get; set; }

        [Display(Name = "booking", ResourceType = typeof(Resource))]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime BookingDate { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
            ErrorMessageResourceName = "zorunluAlan")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        [Display(Name= "checkin", ResourceType =typeof(Resource))]
        public DateTime CheckIn { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
            ErrorMessageResourceName = "zorunluAlan")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        [Display(Name = "checkout", ResourceType = typeof(Resource))]
        public DateTime CheckOut { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
            ErrorMessageResourceName = "zorunluAlan")]
        [Display(Name = "isim", ResourceType = typeof(Resource))]
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
            ErrorMessageResourceName = "zorunluAlan")]
        [Display(Name = "soyisim", ResourceType = typeof(Resource))]
        public string Surname { get; set; }

        [Display(Name = "dogumtarihi", ResourceType = typeof(Resource))]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime? Birthday { get; set; }

        //[Required]
        [Display(Name = "acente", ResourceType = typeof(Resource))]
        public string Agenta { get; set; }

        [Display(Name = "Oda Tercihi")]
        public string WantedRoom { get; set; }

        [Display(Name = "Blok Tercihi")]
        public string WantedBlock { get; set; }

        [Display(Name = "Manzara Tercihi")]
        public string WantedView { get; set; }

        [Display(Name = "Kat Tercihi")]
        public string WantedFloor { get; set; }

        [Display(Name = "Bebek Yatağı")]
        public bool IsBabyBed { get; set; }

        [Display(Name = "Extra Yatak")]
        public bool ExtraBed { get; set; }

        [Display(Name = "guestnote", ResourceType = typeof(Resource))]
        public string GuestNote { get; set; }

        [Display(Name = "tutar", ResourceType = typeof(Resource))]
        public decimal Amount { get; set; }

        [Display(Name = "kur", ResourceType = typeof(Resource))]
        public int CurrencyId { get; set; }

        [Display(Name = "rezyapan", ResourceType = typeof(Resource))]
        public string Person { get; set; }

    }
}
