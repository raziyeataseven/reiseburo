﻿using Dapper.Contrib.Extensions;
using ReiseBuro.Entities.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ReiseBuro.Entities.Models
{
    [Table("UserAction")]
    public class UserAction
    {
        [Dapper.Contrib.Extensions.Key]
        public int Id { get; set; }

        [Display(Name = "kullanici", ResourceType = typeof(Resource))]
        public int UserId { get; set; }

        [Display(Name = "kod", ResourceType = typeof(Resource))]
        public DateTime ActionDate { get; set; }

        [Display(Name = "voucherno", ResourceType = typeof(Resource))]
        public int? ResId { get; set; }

        [Display(Name = "borc", ResourceType = typeof(Resource))]
        public decimal BalanceDue { get; set; }

        [Display(Name = "alacak2", ResourceType = typeof(Resource))]
        public decimal BalanceReceivable { get; set; }

        [Display(Name = "aksiyondurum", ResourceType = typeof(Resource))]
        public string ActionState { get; set; }

        [Display(Name = "hakedilecek", ResourceType = typeof(Resource))]
        public decimal DeservedAmount { get; set; }

        [Display(Name = "kur", ResourceType = typeof(Resource))]
        public int CurrencyId { get; set; }

    
    }
}
