﻿using System.ComponentModel.DataAnnotations;

namespace ReiseBuro.Entities.Models
{
    [Dapper.Contrib.Extensions.Table("Language")]
    public class Language
    {
        [Dapper.Contrib.Extensions.Key]
        public int Id { get; set; }

        [Display(Name ="dil")]
        public string DisplayName { get; set; }

        [Display(Name ="kulturkod")]
        public string Culture { get; set; }
       
    }
}
