﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReiseBuro.Entities.Models
{
    [Table("Log_Transaction")]
    public class Log_Transaction
    {
       [Key]
        public int Id { get; set; }
        public int? UserId { get; set; }
        public string UserName { get; set; }
        public DateTime TransDate { get; set; }
        public string TransComment { get; set; }
    }
}
