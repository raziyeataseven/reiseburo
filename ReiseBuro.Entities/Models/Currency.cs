﻿using Dapper.Contrib.Extensions;
using ReiseBuro.Entities.Resources;
using System.ComponentModel.DataAnnotations;


namespace ReiseBuro.Entities.Models
{
    [Table("Currency")]
    public class Currency
    {
        [Dapper.Contrib.Extensions.Key]
        public int Id { get; set; }
   
        [Required]
        [Display(Name = "isim", ResourceType = typeof(Resource))]
        public string Name { get; set; }

        [Required]
        [Display(Name = "parabirimi", ResourceType = typeof(Resource))]
        public string Xml { get; set; }

        [Required]
        [Display(Name = "bankakodu", ResourceType = typeof(Resource))]
        public string BankCode { get; set; }
    }
}
