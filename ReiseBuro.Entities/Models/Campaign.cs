﻿using Dapper.Contrib.Extensions;
using ReiseBuro.Entities.Resources;
using System;
using System.ComponentModel.DataAnnotations;


namespace ReiseBuro.Entities.Models
{
    [Table("Campaign")]
    public class Campaign
    {
        [Dapper.Contrib.Extensions.Key]
        public int Id { get; set; }
      
        [Display(Name = "aktifmi", ResourceType = typeof(Resource))]
        public bool IsActive { get; set; }

        [Required]
        [Display(Name = "bookingbaslangic", ResourceType = typeof(Resource))]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime BookingBegin { get; set; }

        [Required]
        [Display(Name = "bookingbitis", ResourceType = typeof(Resource))]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime BookingEnd { get; set; }

        [Required]
        [Display(Name = "checkinbas", ResourceType = typeof(Resource))]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime CheckIn { get; set; }

        [Required]
        [Display(Name = "checkinbit", ResourceType = typeof(Resource))]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime CheckOut { get; set; }

        [Display(Name = "ulke", ResourceType = typeof(Resource))]
        public string CountryCode { get; set; }

        [Display(Name = "bonus2", ResourceType = typeof(Resource))]
        public decimal Bonus { get; set; }

        [Display(Name = "mingece", ResourceType = typeof(Resource))]
        public int MinNight { get; set; }

        [Display(Name = "kampanyakod", ResourceType = typeof(Resource))]
        public string CampaignCode { get; set; }
    }
}
