﻿using Dapper.Contrib.Extensions;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace ReiseBuro.Entities.Models
{
    [Table("Param_Mail")]
    public class Param_Mail
    {
        [Dapper.Contrib.Extensions.Key]
        public int Id { get; set; }

        [Required]
        public string MailAddress { get; set; }

        public string DisplayMail { get; set; }

        [Required]
        public string Host { get; set; }

        [Required]
        public string Parola { get; set; }

        [Required]
        public int Port { get; set; }

        public bool Ssl { get; set; }

        public string Stmp { get; set; }

        public string CcList { get; set; }

        public string MailCaption { get { return Helper.SetLanguageVal.GetContent(this.CaptionKeyword); } set { this._display1 = value; } }

        public string MailContent { get { return Helper.SetLanguageVal.GetContent(this.MessageKeyword); } set { this._display2 = value; } }

        public string ContactName { get; set; }

        public string ContactPosition { get; set; }

        public string ContactPhoto { get; set; }

        public string ContactMessage { get; set; }

        public bool IsActive { get; set; }

        public string MessageKeyword { get; set; }
        public string CaptionKeyword { get; set; }


        private string _display1;
        private string _display2;



    }
}
