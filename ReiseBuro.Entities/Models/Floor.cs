﻿using Dapper.Contrib.Extensions;
using System.ComponentModel.DataAnnotations;


namespace ReiseBuro.Entities.Models
{
    [Table("Floor")]
    public class Floor
    {
        [Dapper.Contrib.Extensions.Key]
        public int Id { get; set; }

        [Required]
        public string  Name { get; set; }
    }
}
