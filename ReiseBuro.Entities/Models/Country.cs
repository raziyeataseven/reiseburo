﻿using Dapper.Contrib.Extensions;
using ReiseBuro.Entities.Resources;
using System;
using System.ComponentModel.DataAnnotations;

namespace ReiseBuro.Entities.Models
{
    [Table("Country")]
    public class Country
    {
        [Dapper.Contrib.Extensions.Key]
        public int Id { get; set; }

        [Required]
        [Display(Name="isim", ResourceType =typeof(Resource))]
        public string Name { get; set; }

        [Required]
        [Display(Name = "kod", ResourceType = typeof(Resource))]
        public string Code { get; set; }

        [Required]
        [Display(Name = "kur", ResourceType = typeof(Resource))]
        public int CurrencyId { get; set; }

        [Required]
        [Display(Name = "dil", ResourceType = typeof(Resource))]
        public int LanguageId { get; set; }

    }
}
