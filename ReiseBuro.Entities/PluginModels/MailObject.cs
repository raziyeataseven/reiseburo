﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReiseBuro.Entities.PluginModels
{
    public class MailObject
    {
        public string MailAdres { get; set; }

        public string Subject { get; set; }

        public string HtmlContent { get; set; }

        public Dictionary<string, MemoryStream> AttachmentList { get; set; }

        public string CC_List { get; set; }

    }

}
