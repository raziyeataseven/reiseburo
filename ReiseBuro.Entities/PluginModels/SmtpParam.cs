﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReiseBuro.Entities.PluginModels
{
    public class SmtpParam
    {
        public string DisplayName { get; set; }

        public string DisplayMail { get; set; }

        public string MailAdres { get; set; }

        public string Password { get; set; }

        public string Host { get; set; }

        public int Port { get; set; }

        public bool Ssl { get; set; }
    }
}
