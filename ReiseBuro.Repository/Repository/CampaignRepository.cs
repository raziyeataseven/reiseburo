﻿using ReiseBuro.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReiseBuro.Entities.Models;
using ReiseBuro.Repository.Dapper;
using System.Web;

namespace ReiseBuro.Repository.Repository
{
    public class CampaignRepository : ICampaignRepository
    {
        private IDapperTools dapper { get; set; }
        private readonly ILogTransactionRepository _logRepository;

        public CampaignRepository()
        {
            dapper = new DapperTools(Constants.dapperContext);
            _logRepository=new LogTransactionRepository();
        }

        #region methods

        public Campaign Get(object id)
        {
            return dapper.Get<Campaign>(id);

        }

        public IEnumerable<Campaign> GetAll()
        {
            return dapper.GetAll<Campaign>();
        }

        public long Insert(Campaign obj)
        {
            this.Logs(obj, "Insert");
            return dapper.Insert<Campaign>(obj);

        }

        public bool Update(Campaign obj)
        {
            this.Logs(obj, "Update");
            return dapper.Update<Campaign>(obj);
        }

        public bool Delete(Campaign obj)
        {
            this.Logs(obj, "Delete");
            return dapper.Delete<Campaign>(obj);
        }


        public void Logs(Campaign obj, string method)
        {
            var log = new Log_Transaction
            {
                TransDate = DateTime.Now,
                UserId =0,
                TransComment = method + "; obj => Id= " + obj.Id + ", BookingBegin= " + obj.BookingBegin + ", BookingEnd= " + obj.BookingEnd + ", CheckIn=" + obj.CheckIn + ", CheckOut=" + obj.CheckOut + ", MinNight=" + obj.MinNight + ", Bonus= " + obj.Bonus + ", CoıntryCode= " + obj.CountryCode + ", IsActive= " + obj.IsActive,
            };

            _logRepository.Insert(log);
        }

        public IEnumerable<object> GetGroupedCampaigns(string countrycode)
        {
            return dapper.Query<dynamic>(@"exec GroupCompaignList @CountryCode=@code", new { @code = countrycode });
        }
        #endregion
    }
}
