﻿using ReiseBuro.Entities.Models;
using ReiseBuro.Repository.Dapper;
using ReiseBuro.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReiseBuro.Repository.Repository
{
    public class ImageRepository : IImageRepository
    {

        private IDapperTools dapper { get; set; }
        private readonly ILogTransactionRepository _logRepository;

        public ImageRepository()
        {
            dapper = new DapperTools(Constants.dapperContext);
            _logRepository = new LogTransactionRepository();
        }

        #region methods      
        public bool Delete(Image obj)
        {
            return dapper.Delete<Image>(obj);
        }

        public Image Get(object id)
        {
            return dapper.Get<Image>(id);
        }

        public IEnumerable<Image> GetAll()
        {
            return dapper.GetAll<Image>();
        }

        public long Insert(Image obj)
        {
            return dapper.Insert<Image>(obj);
        }

        public bool Update(Image obj)
        {
            return dapper.Update<Image>(obj);
        }
        #endregion
    }
}
