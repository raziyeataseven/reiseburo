﻿using ReiseBuro.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReiseBuro.Entities.Models;
using ReiseBuro.Repository.Dapper;

namespace ReiseBuro.Repository.Repository
{
    public class LogExceptionRepository : ILogExceptionRepository
    {

        private IDapperTools dapper { get; set; }

        public LogExceptionRepository()
        {
            dapper = new DapperTools(Constants.dapperContext);
        }

        #region methods

        public Log_Exception Get(object id)
        {
            return dapper.Get<Log_Exception>(id);
        }

        public IEnumerable<Log_Exception> GetAll()
        {
            return dapper.GetAll<Log_Exception>();
        }

        public long Insert(Log_Exception obj)
        {
            return dapper.Insert<Log_Exception>(obj);
        }

        public bool Update(Log_Exception obj)
        {
            return dapper.Update<Log_Exception>(obj);
        }

        public bool Delete(Log_Exception obj)
        {
            return dapper.Delete<Log_Exception>(obj);
        }

        #endregion
    }
}
