﻿using ReiseBuro.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReiseBuro.Entities.Models;
using ReiseBuro.Repository.Dapper;

namespace ReiseBuro.Repository.Repository
{
    public class DictionaryRepository : IDictionaryRepository
    {

        private IDapperTools dapper { get; set; }

        public DictionaryRepository()
        {
            dapper = new DapperTools(Constants.dapperContext);
        }


        #region methods
        public bool Delete(Dictionary obj)
        {
            return dapper.Delete<Dictionary>(obj);
        }

        public Dictionary Get(object id)
        {
            return dapper.Get<Dictionary>(id);
        }

        public IEnumerable<Dictionary> GetAll()
        {
            return dapper.GetAll<Dictionary>();
        }

        public long Insert(Dictionary obj)
        {
            return dapper.Insert<Dictionary>(obj);
        }

        public bool Update(Dictionary obj)
        {
            return dapper.Update<Dictionary>(obj);
        }
        #endregion
    }
}
