﻿using ReiseBuro.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReiseBuro.Entities.Models;
using ReiseBuro.Repository.Dapper;

namespace ReiseBuro.Repository.Repository
{
    public class UserRepository : IUserRepository
    {
        private IDapperTools dapper { get; set; }
        private readonly ILogTransactionRepository _logRepository;
        public UserRepository()
        {
            dapper = new DapperTools(Constants.dapperContext);
            _logRepository = new LogTransactionRepository();
        }

        #region methods

        public User Get(object id)
        {
            return dapper.Get<User>(id);
        }

        public IEnumerable<User> GetAll()
        {
            return dapper.GetAll<User>();
        }

        public long Insert(User obj)
        {
            this.Logs(obj, "Insert", "");
            return dapper.Insert<User>(obj);
        }

        public bool Update(User obj)
        {
            this.Logs(obj, "Update","");
            return dapper.Update<User>(obj);
        }

        public bool Delete(User obj)
        {
            this.Logs(obj, "Delete", "");
            return dapper.Delete<User>(obj);
        }

        public User GetUserLoginObj(string username, string password)
        {
            var user=dapper.Query<User>(@"select * from Users where Username=@username and Password=@password", 
                new { @username = username, @password = password }).FirstOrDefault();
            if (user == null)
            {
                this.Logs("GetUserLoginObj", "Invalid Login Attempt. Username=" + username + ",Password=" + password);

            }
            else
            {
                this.Logs(user, "GetUserLoginObj", "Login Successful.");
            }
            return user;
        }


        public void Logs(User obj, string method, string message)
        {
            var log = new Log_Transaction
            {
                TransDate = DateTime.Now,
                TransComment = method + "; obj => Id= " + obj.Id + ", UserName= " + obj.UserName + ", Email= " + obj.Email + ", Role=" + obj.Role + ", PersonInCharge=" + obj.PersonInCharge +"; message => "+message
            };

            _logRepository.Insert(log);
        }

        public void Logs(string method, string message)
        {
            var log = new Log_Transaction
            {
                TransDate = DateTime.Now,
                TransComment = method + "; message => "+ message
            };

            _logRepository.Insert(log);
        }

        public User GetUserbyUsername(string username)
        {
            return dapper.Query<User>(@"Select * from Users where UserName=@name", new { @name = username }).FirstOrDefault();
        }

        #endregion

    }
}
