﻿using ReiseBuro.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReiseBuro.Entities.Models;
using ReiseBuro.Repository.Dapper;
using System.Web;

namespace ReiseBuro.Repository.Repository
{
    public class UserActionRepository : IUserActionRepository
    {
        private IDapperTools dapper { get; set; }
        private readonly ILogTransactionRepository _logRepository = new LogTransactionRepository();

        public UserActionRepository()
        {
            dapper = new DapperTools(Constants.dapperContext);
        }

        #region methods

        public UserAction Get(object id)
        {
            return dapper.Get<UserAction>(id);
        }

        public IEnumerable<UserAction> GetAll()
        {
            return dapper.GetAll<UserAction>();
        }

        public long Insert(UserAction obj)
        {
            return dapper.Insert<UserAction>(obj);
        }

        public bool Update(UserAction obj)
        {
            this.Logs(obj, "Update");
            return dapper.Update<UserAction>(obj);
        }
        public bool Delete(UserAction obj)
        {
            this.Logs(obj,"Delete");
            return dapper.Delete<UserAction>(obj);
        }


        public void Logs(UserAction obj, string method)
        {
            var log = new Log_Transaction
            {
                TransDate = DateTime.Now,
                UserId =obj.UserId,
                TransComment = method+"; obj => Id= " + obj.Id + ", UserId= " + obj.UserId + ", ResId= " + obj.ResId + ", BalanceDue=" + obj.BalanceDue + ", BalanceReceivable=" + obj.BalanceReceivable + ", ActionState=" + obj.ActionState
            };

            _logRepository.Insert(log);
        }

        public IEnumerable<UserAction> GetAllByResId(int? id)
        {
            return dapper.Query<UserAction>(@"select * from UserAction where ResId=@id", new { @id = id }).ToList();
        }
        #endregion

    }
}
