﻿using ReiseBuro.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReiseBuro.Entities.Models;
using ReiseBuro.Repository.Dapper;

namespace ReiseBuro.Repository.Repository
{
    public class CurrencyRepository : ICurrencyRepository
    {
        private IDapperTools dapper { get; set; }

        public CurrencyRepository()
        {
            dapper = new DapperTools(Constants.dapperContext);
        }


        #region methods
        
        public Currency Get(object id)
        {
            return dapper.Get<Currency>(id);
        }

        public IEnumerable<Currency> GetAll()
        {
            return dapper.GetAll<Currency>();
        }

        public long Insert(Currency obj)
        {
            return dapper.Insert<Currency>(obj);
        }

        public bool Update(Currency obj)
        {
            return dapper.Update<Currency>(obj);
        }

        public bool Delete(Currency obj)
        {
            return dapper.Delete<Currency>(obj);
        }

        #endregion
    }
}
