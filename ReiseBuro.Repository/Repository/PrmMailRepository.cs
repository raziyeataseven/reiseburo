﻿using ReiseBuro.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReiseBuro.Entities.Models;
using ReiseBuro.Repository.Dapper;

namespace ReiseBuro.Repository.Repository
{
    public class PrmMailRepository : IPrmMailRepository
    {

        private IDapperTools dapper { get; set; }

        public PrmMailRepository()
        {
            dapper = new DapperTools(Constants.dapperContext);
        }

        #region methods
        public Param_Mail Get(object id)
        {
            return dapper.Get<Param_Mail>(id);
        }

        public IEnumerable<Param_Mail> GetAll()
        {
            return dapper.GetAll<Param_Mail>();
        }

        public long Insert(Param_Mail obj)
        {
            return dapper.Insert<Param_Mail>(obj);
        }

        public bool Update(Param_Mail obj)
        {
            return dapper.Update<Param_Mail>(obj);
        }

        public bool Delete(Param_Mail obj)
        {
            return dapper.Delete<Param_Mail>(obj);
        }

        public Param_Mail GetActiveMail()
        {
            return dapper.Query<Param_Mail>(@"select * from Param_Mail where IsActive=1").First();
        }
        #endregion
    }
}
