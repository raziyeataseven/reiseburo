﻿using ReiseBuro.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReiseBuro.Entities.Models;
using ReiseBuro.Repository.Dapper;

namespace ReiseBuro.Repository.Repository
{
    public class CountryRepository : ICountryRepository
    {

        private IDapperTools dapper { get; set; }

        public CountryRepository()
        {
            dapper = new DapperTools(Constants.dapperContext);
        }


        #region methods
      
        public Country Get(object id)
        {
            return dapper.Get<Country>(id);
        }

        public IEnumerable<Country> GetAll()
        {
            return dapper.GetAll<Country>();
        }

        public long Insert(Country obj)
        {
            return dapper.Insert<Country>(obj);
        }

        public bool Update(Country obj)
        {
            return dapper.Update<Country>(obj);
        }

        public bool Delete(Country obj)
        {
            return dapper.Delete<Country>(obj);
        }

        public Country GetByCountryCode(string code)
        {
            return dapper.Query<Country>(@"Select * From Country where Code=@code", new { @code = code }).First();
        }

        #endregion

    }
}
