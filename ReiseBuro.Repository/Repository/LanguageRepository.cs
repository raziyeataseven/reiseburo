﻿using ReiseBuro.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReiseBuro.Entities.Models;
using ReiseBuro.Repository.Dapper;

namespace ReiseBuro.Repository.Repository
{
    public class LanguageRepository : ILanguageRepository
    {
        private IDapperTools dapper { get; set; }

        public LanguageRepository()
        {
            dapper = new DapperTools(Constants.dapperContext);
        }


        public bool Delete(Language obj)
        {
            return dapper.Delete<Language>(obj);
        }

        public Language Get(object id)
        {
            return dapper.Get<Language>(id);
        }

        public IEnumerable<Language> GetAll()
        {
            return dapper.GetAll<Language>();
        }

        public long Insert(Language obj)
        {
            return dapper.Insert<Language>(obj);
        }

        public bool Update(Language obj)
        {
            return dapper.Update<Language>(obj);
        }
    }
}
