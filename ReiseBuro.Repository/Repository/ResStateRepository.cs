﻿using ReiseBuro.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReiseBuro.Entities.Models;
using ReiseBuro.Repository.Dapper;

namespace ReiseBuro.Repository.Repository
{
    public class ResStateRepository : IResStateRepository
    {
        private IDapperTools dapper { get; set; }

        public ResStateRepository()
        {
            dapper = new DapperTools(Constants.dapperContext);
        }


        #region methods
        
        public ResState Get(object id)
        {
            return dapper.Get<ResState>(id);
        }

        public IEnumerable<ResState> GetAll()
        {
            return dapper.GetAll<ResState>();
        }

        public long Insert(ResState obj)
        {
            return dapper.Insert<ResState>(obj);
        }

        public bool Update(ResState obj)
        {
            return dapper.Update<ResState>(obj);
        }

        public bool Delete(ResState obj)
        {
            return dapper.Delete<ResState>(obj);
        }

       
        #endregion

    }
}
