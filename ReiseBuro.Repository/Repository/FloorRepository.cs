﻿using ReiseBuro.Repository.IRepository;
using System;
using System.Collections.Generic;
using ReiseBuro.Entities.Models;
using ReiseBuro.Repository.Dapper;

namespace ReiseBuro.Repository.Repository
{
    public class FloorRepository : IFloorRepository
    {
        private IDapperTools dapper { get; set; }

        public FloorRepository()
        {
            dapper = new DapperTools(Constants.dapperContext);
        }

        #region methods
        public bool Delete(Floor obj)
        {
            return dapper.Delete<Floor>(obj);
        }

        public Floor Get(object id)
        {
            return dapper.Get<Floor>(id);
        }

        public IEnumerable<Floor> GetAll()
        {
            return dapper.GetAll<Floor>();
        }

        public long Insert(Floor obj)
        {
            return dapper.Insert<Floor>(obj);
        }

        public bool Update(Floor obj)
        {
            return dapper.Update<Floor>(obj);
        }
        #endregion
    }

    public class LandscapeRepository : ILandscapeRepository
    {
        private IDapperTools dapper { get; set; }

        public LandscapeRepository()
        {
            dapper = new DapperTools(Constants.dapperContext);
        }

        public bool Delete(Landscape obj)
        {
            return dapper.Delete<Landscape>(obj);
        }

        public Landscape Get(object id)
        {
            return dapper.Get<Landscape>(id);
        }

        public IEnumerable<Landscape> GetAll()
        {
            return dapper.GetAll<Landscape>();
        }

        public long Insert(Landscape obj)
        {
            return dapper.Insert<Landscape>(obj);
        }

        public bool Update(Landscape obj)
        {
            return dapper.Update<Landscape>(obj);
        }
    }
}
