﻿using ReiseBuro.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReiseBuro.Entities.Models;
using ReiseBuro.Repository.Dapper;

namespace ReiseBuro.Repository.Repository
{
    public class LogTransactionRepository : ILogTransactionRepository
    {

        private IDapperTools dapper { get; set; }

        public LogTransactionRepository()
        {
            dapper = new DapperTools(Constants.dapperContext);
        }

        #region methods

        public Log_Transaction Get(object id)
        {
            return dapper.Get<Log_Transaction>(id);
        }

        public IEnumerable<Log_Transaction> GetAll()
        {
            return dapper.GetAll<Log_Transaction>();
        }

        public long Insert(Log_Transaction obj)
        {
            return dapper.Insert<Log_Transaction>(obj);
        }

        public bool Update(Log_Transaction obj)
        {
            return dapper.Update<Log_Transaction>(obj);
        }

        public bool Delete(Log_Transaction obj)
        {
            return dapper.Delete<Log_Transaction>(obj);
        }

        #endregion
    }
}
