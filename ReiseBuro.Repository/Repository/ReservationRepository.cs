﻿using ReiseBuro.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReiseBuro.Entities.Models;
using ReiseBuro.Repository.Dapper;
using ReiseBuro.Repository.Helper;

namespace ReiseBuro.Repository.Repository
{
    public class ReservationRepository : IReservationRepository
    {
        private IDapperTools dapper { get; set; }

        public ReservationRepository()
        {
            dapper = new DapperTools(Constants.dapperContext);
        }

        #region methods
       
        public Reservation Get(object id)
        {
            return dapper.Get<Reservation>(id);
        }

        public IEnumerable<Reservation> GetAll()
        {
            return dapper.GetAll<Reservation>();
        }

        public long Insert(Reservation obj)
        {
            return dapper.Insert<Reservation>(obj);
        }

        public bool Update(Reservation obj)
        {
            return dapper.Update<Reservation>(obj);
        }

        public bool Delete(Reservation obj)
        {
            return dapper.Delete<Reservation>(obj);
        }

        public List<string> GetAllVoucher()
        {
            return dapper.Query<string>(@"select VoucherNo from Reservation").ToList();
           
        }
        #endregion

    }
}
