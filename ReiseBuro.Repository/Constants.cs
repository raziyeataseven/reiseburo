﻿using ReiseBuro.Repository.Dapper;
using System.Configuration;

namespace ReiseBuro.Repository
{
    public class Constants
    {
        public static IDapperContext dapperContext => new DapperContext(ConfigurationManager.ConnectionStrings["RbConnection"].ConnectionString);

    }
}
