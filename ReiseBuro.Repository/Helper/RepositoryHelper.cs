﻿using ReiseBuro.Repository.IRepository;
using ReiseBuro.Repository.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReiseBuro.Repository.Helper
{

    public class RepositoryHelper
    {
        private static readonly IReservationRepository _resRepository = new ReservationRepository();

        private static Random random = new Random();

        public static string GenerateVoucher()
        {

            int lengthOfVoucher = 10;
            List<string> generatedVouchers = _resRepository.GetAllVoucher();
            char[] keys = "ABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890".ToCharArray();

            voucher:
            var voucher ="RV_"+ Enumerable.Range(1, lengthOfVoucher).Select(k => keys[random.Next(0, keys.Length - 1)]).Aggregate("", (e, c) => e + c);
            if (!generatedVouchers.Contains(voucher))
            {
                return Enumerable.Range(1, lengthOfVoucher).Select(k => keys[random.Next(0, keys.Length - 1)]).Aggregate("", (e, c) => e + c);
            }
            else
                goto voucher;

        }


    }

}
