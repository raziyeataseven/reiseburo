﻿using ReiseBuro.Entities.PluginModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ReiseBuro.Repository.Plugins
{
    public interface ISmtpMail
    {
        void SendMail(MailObject mail, SmtpParam param);
    }

    public class SmtpMail : ISmtpMail
    {

        public void SendMail(MailObject mail, SmtpParam param)
        {
            try
            {
                //System.Threading.Thread.Sleep(1 * 1000);
                MailMessage ePosta = new MailMessage();
                ePosta.From = new MailAddress(param.MailAdres, param.DisplayName);
                //ePosta.To.Add(mail.MailAdres);
                if (!string.IsNullOrEmpty(mail.CC_List))
                {
                    //ePosta.CC.Add(mail.CC_List);
                    foreach (string item in mail.CC_List.Replace(" ", "").Replace(";", "; ").Split(';'))
                    {
                        ePosta.CC.Add(item);
                    }
                }
                ePosta.To.Add(new MailAddress(mail.MailAdres));
                ePosta.Priority = MailPriority.Normal;
                ePosta.Subject = mail.Subject;
                ePosta.AlternateViews.Add(CreateHtmlView(mail.HtmlContent));
                ePosta.IsBodyHtml = true;
                if (mail.AttachmentList != null)
                {
                    foreach (var item in mail.AttachmentList)
                    {
                        ePosta.Attachments.Add(new Attachment(item.Value, item.Key));
                    }
                }


                SmtpClient ss = new SmtpClient(param.Host, param.Port);
                ss.EnableSsl = param.Ssl;
                ss.DeliveryMethod = SmtpDeliveryMethod.Network;
                ss.UseDefaultCredentials = false;
                ss.Credentials = new NetworkCredential(param.MailAdres, param.Password);
                ss.Send(ePosta);

            }
            catch (Exception ex)
            {
                //ex.Save();
                throw ex;
            }
        }

        public async Task SendMailAsync(MailObject mail, SmtpParam param)
        {
            try
            {

                System.Threading.Thread.Sleep(1 * 1000);

                MailMessage ePosta = new MailMessage();
                ePosta.From = new MailAddress(param.MailAdres, param.DisplayName);
                ePosta.To.Add(mail.MailAdres);
                ePosta.Priority = MailPriority.Normal;
                ePosta.Subject = mail.Subject;
                ePosta.AlternateViews.Add(CreateHtmlView(mail.HtmlContent));
                ePosta.IsBodyHtml = true;
                if (mail.AttachmentList != null)
                {
                    foreach (var item in mail.AttachmentList)
                    {
                        ePosta.Attachments.Add(new Attachment(item.Value, item.Key));
                    }
                }

                using (SmtpClient ss = new SmtpClient(param.Host, param.Port))
                {
                    ss.EnableSsl = param.Ssl;
                    ss.DeliveryMethod = SmtpDeliveryMethod.Network;
                    ss.UseDefaultCredentials = false;
                    ss.Credentials = new NetworkCredential(param.MailAdres, param.Password);
                    ss.SendCompleted += Ss_SendCompleted;
                    ss.SendAsync(ePosta, ePosta);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void Ss_SendCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                //handle error
            }
            else if (e.Cancelled)
            {
                //handle cancelled
            }
            else
            {
                MailMessage message = (MailMessage)e.UserState;
            }
        }


        //private List<Uri> ImageUriList(string htmlSource)
        //{
        //    List<Uri> links = new List<Uri>();
        //    string regexImgSrc = @"<img[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";
        //    MatchCollection matchesImgSrc = Regex.Matches(htmlSource, regexImgSrc, RegexOptions.IgnoreCase | RegexOptions.Singleline);
        //    foreach (Match m in matchesImgSrc)
        //    {
        //        string href = m.Groups[1].Value;
        //        links.Add(new Uri(href));
        //    }
        //    return links;
        //}

        private List<string> ImageUriList(string htmlSource)
        {
            List<string> links = new List<string>();
            string regexImgSrc = @"<img[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";
            MatchCollection matchesImgSrc = Regex.Matches(htmlSource, regexImgSrc, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            foreach (Match m in matchesImgSrc)
            {
                string href = m.Groups[1].Value;
                links.Add(href);
            }
            return links;
        }

        private AlternateView CreateHtmlView(string htmlContent)
        {
            //AlternateView view = AlternateView.CreateAlternateViewFromString(htmlContent, Encoding.UTF8, MediaTypeNames.Text.Html);


            //List<Uri> uris = ImageUriList(htmlContent);
            //List<LinkedResource> resources = new List<LinkedResource>();

            //foreach (Uri uri in uris)
            //{
            //    string base64Data = uri.OriginalString.Substring(uri.OriginalString.IndexOf("base64,")).Replace("base64,","");

            //    LinkedResource resource = new LinkedResource(new MemoryStream(System.Convert.FromBase64String(base64Data)));
            //    resource.ContentId = uris.IndexOf(uri).ToString() + ".png";
            //    //view.LinkedResources.Add(resource);

            //    resources.Add(resource);

            //    htmlContent = htmlContent.Replace(uri.OriginalString, "cid:" + uris.IndexOf(uri).ToString() + ".png");
            //}

            List<string> uris = ImageUriList(htmlContent);
            List<LinkedResource> resources = new List<LinkedResource>();

            foreach (string uri in uris)
            {

                if (uri.IndexOf("base64,") < 0)
                {
                    continue;
                }

                string base64Data = uri.Substring(uri.IndexOf("base64,")).Replace("base64,", "");

                LinkedResource resource = new LinkedResource(new MemoryStream(System.Convert.FromBase64String(base64Data)));
                resource.ContentId = uris.IndexOf(uri).ToString() + ".png";
                //view.LinkedResources.Add(resource);

                resources.Add(resource);

                htmlContent = htmlContent.Replace(uri, "cid:" + uris.IndexOf(uri).ToString() + ".png");
            }




            AlternateView view = AlternateView.CreateAlternateViewFromString(htmlContent, Encoding.UTF8, MediaTypeNames.Text.Html);
            foreach (LinkedResource item in resources)
            {
                view.LinkedResources.Add(item);
            }

            return view;
        }


    }

}
