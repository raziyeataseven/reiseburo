﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReiseBuro.Repository
{
    public interface IRepository<TEntity> where TEntity:class
    {
        TEntity Get(object id);

        IEnumerable<TEntity> GetAll();

        long Insert(TEntity obj);

        bool Update(TEntity obj);

        bool Delete(TEntity obj);
    }
}
