﻿using ReiseBuro.Entities.Models;
using System.Collections.Generic;

namespace ReiseBuro.Repository.IRepository
{
    public interface ICampaignRepository:IRepository<Campaign>
    {
        IEnumerable<object> GetGroupedCampaigns(string countrycode);
    }
}
