﻿using ReiseBuro.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReiseBuro.Repository.IRepository
{
    public interface IUserRepository:IRepository<User>
    {
        User GetUserLoginObj(string username, string password);
        User GetUserbyUsername(string username);
    }
}
