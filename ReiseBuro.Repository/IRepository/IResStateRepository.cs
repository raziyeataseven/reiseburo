﻿using ReiseBuro.Entities.Models;
using System;


namespace ReiseBuro.Repository.IRepository
{
    public interface IResStateRepository : IRepository<ResState>
    {
      
    }
}
