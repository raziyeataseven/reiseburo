﻿using ReiseBuro.Entities.Models;

namespace ReiseBuro.Repository.IRepository
{
    public interface ILanguageRepository:IRepository<Language>
    {
    }
}
