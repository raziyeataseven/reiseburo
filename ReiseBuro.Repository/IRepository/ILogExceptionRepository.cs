﻿using ReiseBuro.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReiseBuro.Repository.IRepository
{
    public interface ILogExceptionRepository:IRepository<Log_Exception>
    {
    }
}
