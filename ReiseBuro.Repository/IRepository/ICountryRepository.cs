﻿using ReiseBuro.Entities.Models;


namespace ReiseBuro.Repository.IRepository
{
    public interface ICountryRepository:IRepository<Country>
    {
        Country GetByCountryCode(string code);
    }
}
