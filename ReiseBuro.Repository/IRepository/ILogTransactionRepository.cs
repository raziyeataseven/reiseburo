﻿using ReiseBuro.Entities.Models;


namespace ReiseBuro.Repository.IRepository
{
    public interface ILogTransactionRepository:IRepository<Log_Transaction>
    {
    }
}
