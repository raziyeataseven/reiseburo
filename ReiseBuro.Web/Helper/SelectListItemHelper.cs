﻿using ReiseBuro.Repository.IRepository;
using ReiseBuro.Repository.Repository;
using ReiseBuro.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReiseBuro.Web.Helper
{
    public class SelectListItemHelper
    {
        private static readonly IDictionaryRepository _dictionaryRepository = new DictionaryRepository();
        private static readonly IResStateRepository _resStateRepository = new ResStateRepository();
        private static readonly IUserRepository _userRepository = new UserRepository();
        private static readonly ICountryRepository _countryRepository = new CountryRepository();
        private static readonly ICurrencyRepository _currencyRepository = new CurrencyRepository();
        private static readonly ILanguageRepository _languageRepository = new LanguageRepository();

        public static List<SelectListItem> DictionaryList()
        {
            var objList = _dictionaryRepository.GetAll()
                .GroupBy(f => f.D_Key)
                .Select(a => a.FirstOrDefault());
            var items = new List<SelectListItem>();

            foreach (var item in objList)
            {
                items.Add(new SelectListItem
                {
                    Value = item.D_Key,
                    Text = "Key: " + item.D_Key
                });

            }
            return items;
        }


        public static List<SelectListItem> CultureKeyValue()
        {
            var items = new List<SelectListItem>();

            var languages = _languageRepository.GetAll();

            foreach (var lang in languages)
            {
                items.Add(new SelectListItem { Text = lang.DisplayName, Value = lang.Culture.ToLower() });
            }

            return items;
        }

        public static List<SelectListItem> StateKeyValue()
        {
            var items = new List<SelectListItem>();
            var states = _resStateRepository.GetAll();
            foreach (var item in states)
            {
                items.Add(new SelectListItem
                {
                    Text = item.ConfirmState,
                    Value = item.ConfirmCode
                });
            }

            return items;
        }

        public static List<SelectListItem> GetUsersKeyValue()
        {
            var items = new List<SelectListItem>();
            var users = _userRepository.GetAll();

            foreach (var item in users)
            {
                items.Add(new SelectListItem
                {
                    Value = item.Id.ToString(),
                    Text = item.UserName
                });
            }

            return items;
        }

        public static List<SelectListItem> GetCountryKeyValue()
        {
            var objList = _countryRepository.GetAll();
            var items = new List<SelectListItem>();
            foreach (var item in objList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.Name,
                    Value = item.Code
                });
            }

            return items;
        }

        public static List<SelectListItem> CurrencyList()
        {
            var items = new List<SelectListItem>();
            var currencies = _currencyRepository.GetAll();

            foreach (var currency in currencies)
            {
                items.Add(new SelectListItem
                {
                    Text = currency.Xml,
                    Value = currency.Id.ToString()
                });
            }

            return items;
        }

        public static List<SelectListItem> CurrencyList(string countrycode)
        {
            var items = new List<SelectListItem>();
            var country = _countryRepository.GetByCountryCode(countrycode);
            var currency = _currencyRepository.Get(country.CurrencyId);

            items.Add(new SelectListItem
            {
                Text = currency.Xml,
                Value = currency.Id.ToString()
            });


            return items;
        }

        public static List<SelectListItem> GetLanguageKeyValue()
        {
            var items = new List<SelectListItem>();
            var langs = _languageRepository.GetAll();
            foreach (var item in langs)
            {
                items.Add(new SelectListItem
                {
                    Text = item.DisplayName,
                    Value = item.Id.ToString()
                });
            }

            return items;
        }

        public static List<SelectListItem> GetRoles()
        {
            var items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Admin", Value = "Administrator" });
            items.Add(new SelectListItem { Text = "Kullanıcı", Value = "Kullanici" });
            return items;
        }

        public static IEnumerable<ResStatusViewModel> GetDisplayResStatus()
        {
            var displayStatus = new List<ResStatusViewModel>();
            var statusModel = _resStateRepository.GetAll();
            foreach (var item in statusModel)
            {
                displayStatus.Add(new ResStatusViewModel
                {
                    ActionKeyword = item.ActionKeyword,
                    ConfirmCode = item.ConfirmCode,
                    ConfirmState = item.ConfirmState,
                    Id = item.Id
                });
               
            }

            return displayStatus.ToList();

        }
    }
}