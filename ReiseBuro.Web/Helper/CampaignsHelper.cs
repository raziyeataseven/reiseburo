﻿using ReiseBuro.Entities.Models;
using ReiseBuro.Repository.IRepository;
using ReiseBuro.Repository.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace ReiseBuro.Web.Helper
{
    public class CampaignsHelper
    {
        private static ICampaignRepository _campaignsRepository = new CampaignRepository();

       

        internal static Campaign GetBonusAmount(Reservation reservation)
        {          
            try
            {
                var user = (User)HttpContext.Current.Session["user"];

                var minNight = (reservation.CheckOut - reservation.CheckIn).TotalDays;
                IEnumerable<Campaign> campaigns = _campaignsRepository.GetAll().Where(f => f.IsActive == true &&
                f.CountryCode == user.CountryCode &&
                f.MinNight <= minNight &&
                (f.BookingBegin <= reservation.BookingDate ||
                f.BookingEnd >= reservation.BookingDate) &&
                f.CheckIn <= reservation.CheckIn &&
                f.CheckOut >= reservation.CheckIn).ToList();

                var maxId = campaigns.Max(i => i.Id);
                var validCampaign = new Campaign();
              
                if (campaigns != null)
                {
                    validCampaign = campaigns.First(f => f.Id == maxId);
                }
                else
                {
                    validCampaign.Bonus = decimal.Zero;
                }

                //var actionObj = new UserAction
                //{
                //    ActionDate = DateTime.Now,
                //    ResId = reservation.Id,
                //    UserId = reservation.UserId
                //};

                //switch (state)
                //{
                //    case aa:
                //        actionObj.BalanceDue = 0;
                //        actionObj.BalanceReceivable = 0;
                //        break;
                //    case waitingStatus:
                //        actionObj.BalanceDue = 0;
                //        actionObj.BalanceReceivable = 0;
                //        actionObj.DeservedAmount = validCampaign.Bonus;
                //        break;
                //    case confirmStatus:
                //        actionObj.BalanceDue = 0;
                //        actionObj.BalanceReceivable = 0;
                //        actionObj.DeservedAmount = validCampaign.Bonus;
                //        break;
                //    default:
                //        break;
                //}

                return validCampaign;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}