﻿using ReiseBuro.Entities.Models;
using ReiseBuro.Repository.IRepository;
using ReiseBuro.Repository.Repository;
using ReiseBuro.Web.Authentication;
using System.Web;

namespace ReiseBuro.Web.Helper
{
    [ReiseAuthorize]
    public static class UserInfoHelper
    {
        private readonly static ICountryRepository _countryRepository = new CountryRepository();
        private readonly static ICurrencyRepository _currencyRepository = new CurrencyRepository();
        private readonly static IUserRepository _userRepository = new UserRepository();

        public static string UserFullname()
        {
            var user = (User)HttpContext.Current.Session?["user"];
            if (user == null)
                return "";
            return user.Name + " " + user.Surname;

        }

        public static string UserMailAddress()
        {
            var user = (User)HttpContext.Current.Session?["user"];
            if (user == null)
                return "";
            return user.Email;
        }

        public static string UserRole()
        {
            var user = (User)HttpContext.Current.Session?["user"];
            if (user == null)
                return "";
            return user.Role;
        }

        public static int GetUserid()
        {
            var user = (User)HttpContext.Current.Session?["user"];          
            return user.Id;
        }
        

        public static string GetUserCurrency()
        {

            var user = (User)HttpContext.Current.Session?["user"];
            var country = _countryRepository.GetByCountryCode(user.CountryCode);
            var currency = _currencyRepository.Get(country.CurrencyId);

            return currency.Xml;
        }

        public static string GetUserCurrency(int userId)
        {
            var user = _userRepository.Get(userId);
            var country = _countryRepository.GetByCountryCode(user.CountryCode);
            var currency = _currencyRepository.Get(country.CurrencyId);

            return currency.Xml;
        }

        public static string GetUserCountryCode()
        {
            var user = (User)HttpContext.Current.Session["user"];
            return user.CountryCode;
        }

    }
}