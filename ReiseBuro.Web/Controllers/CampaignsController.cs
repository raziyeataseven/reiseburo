﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ReiseBuro.Entities.Models;
using ReiseBuro.Web.Models;
using ReiseBuro.Repository.IRepository;
using ReiseBuro.Web.Authentication;
using ReiseBuro.Web.Helper;
using Itenso.TimePeriod;

namespace ReiseBuro.Web.Controllers
{
    [ReiseAuthorize(Roles = "Administrator,Kullanici")]
    public class CampaignsController : Controller
    {
        private readonly ICampaignRepository _campaignRepository;
        private readonly ICountryRepository _countryRepoistory;
        private readonly ICurrencyRepository _currencyRepoistory;

        public CampaignsController(ICampaignRepository campaignRepository, ICountryRepository countryRepository, ICurrencyRepository currencyRepoistory)
        {
            this._campaignRepository = campaignRepository;
            this._countryRepoistory = countryRepository;
            this._currencyRepoistory = currencyRepoistory;
        }

        // GET: Campaigns
        public ActionResult Index(string countryCode)
        {
            var countries = _countryRepoistory.GetAll();
            var currency = _currencyRepoistory.GetAll();
            ViewBag.Countries = countries;

            Dictionary<string, string> currencies = new Dictionary<string, string>();
            foreach (var item in countries)
            {
                currencies.Add(item.Code, currency.FirstOrDefault(f => f.Id == item.CurrencyId).Xml);
            }
            ViewBag.CurrencyDict = currencies;
            var user = (User)Session["user"];
            IEnumerable<dynamic> CampainsWithRange=null;            
            
            if (UserInfoHelper.UserRole() == "Administrator")
            {
                if (string.IsNullOrEmpty(countryCode))
                    CampainsWithRange = _campaignRepository.GetGroupedCampaigns(user.CountryCode);
                else
                    CampainsWithRange = _campaignRepository.GetGroupedCampaigns(countryCode);           

                ViewBag.campaignsOrdered = CampainsWithRange;
                ViewBag.CountrySelectlist = Helper.SelectListItemHelper.GetCountryKeyValue();
                return View(_campaignRepository.GetAll().ToList());
            }

            else
            {
                CampainsWithRange = _campaignRepository.GetGroupedCampaigns(user.CountryCode);
                ViewBag.campaignsOrdered = CampainsWithRange;
                var users = (User)Session["user"];
                return View(_campaignRepository.GetAll().Where(i => i.CountryCode == users.CountryCode));
            }
        }


        // GET: Campaigns/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Campaign campaign = _campaignRepository.Get(id);
            if (campaign == null)
            {
                return HttpNotFound();
            }
            ViewBag.Country = _countryRepoistory.GetByCountryCode(campaign.CountryCode).Name;
            return View(campaign);
        }

        // GET: Campaigns/Create
        [ReiseAuthorize(Roles = "Administrator")]
        public ActionResult Create()
        {
            ViewBag.Countries = GetCountryKeyValue();
            return View();
        }

        private List<SelectListItem> GetCountryKeyValue()
        {
            var items = new List<SelectListItem>();
            var countries = _countryRepoistory.GetAll();
            foreach (var country in countries)
            {
                items.Add(new SelectListItem
                {
                    Text = country.Name,
                    Value = country.Code
                });
            }
            return items;
        }

        // POST: Campaigns/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [ReiseAuthorize(Roles = "Administrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IsActive,BookingBegin,BookingEnd,CheckIn,CheckOut,CountryCode,Bonus,MinNight,CampaignCode")] Campaign campaign)
        {
            if (ModelState.IsValid)
            {
                var oldCampaigns = _campaignRepository.GetAll().Where(f => f.BookingBegin == campaign.BookingBegin &&
                                                                      f.BookingEnd == campaign.BookingEnd &&
                                                                      f.CheckIn == campaign.CheckIn &&
                                                                      f.CheckOut == campaign.CheckOut &&
                                                                      f.CountryCode == campaign.CountryCode);

                if (oldCampaigns != null)
                {
                    foreach (var item in oldCampaigns)
                    {
                        item.IsActive = false;
                        _campaignRepository.Update(item);
                    }
                }

                _campaignRepository.Insert(campaign);
                return RedirectToAction("Index");
            }
            ViewBag.Countries = GetCountryKeyValue();
            return View(campaign);
        }

        // GET: Campaigns/Edit/5
        [ReiseAuthorize(Roles = "Administrator")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Campaign campaign = _campaignRepository.Get(id);
            if (campaign == null)
            {
                return HttpNotFound();
            }
            ViewBag.CountryList = GetCountryKeyValue();
            return View(campaign);
        }

        // POST: Campaigns/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [ReiseAuthorize(Roles = "Administrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,IsActive,BookingBegin,BookingEnd,CheckIn,CheckOut,CountryCode,Bonus,MinNight,CampaignCode")] Campaign campaign)
        {
            if (ModelState.IsValid)
            {
                _campaignRepository.Update(campaign);
                return RedirectToAction("Index");
            }
            return View(campaign);
        }

        // GET: Campaigns/Delete/5
        [ReiseAuthorize(Roles = "Administrator")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Campaign campaign = _campaignRepository.Get(id);
            if (campaign == null)
            {
                return HttpNotFound();
            }
            ViewBag.Country = _countryRepoistory.GetByCountryCode(campaign.CountryCode).Name;
            return View(campaign);
        }

        // POST: Campaigns/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [ReiseAuthorize(Roles = "Administrator")]
        public ActionResult DeleteConfirmed(int id)
        {
            Campaign campaign = _campaignRepository.Get(id);
            _campaignRepository.Delete(campaign);
            return RedirectToAction("Index");
        }


    }
}
