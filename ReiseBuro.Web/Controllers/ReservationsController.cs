﻿using System;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ReiseBuro.Entities.Models;
using ReiseBuro.Web.Authentication;
using ReiseBuro.Repository.IRepository;
using ReiseBuro.Web.Helper;

namespace ReiseBuro.Web.Controllers
{
    [ReiseAuthorize(Roles = "Administrator,Kullanici")]
    public class ReservationsController : Controller
    {
        private readonly IReservationRepository _resRepositroy;
        private readonly ICurrencyRepository _currencyRepository;
        private readonly IUserActionRepository _userActionRepository;
        private readonly IUserRepository _userRepository;
        private readonly ICountryRepository _countryRepository;

        private readonly static string WaitingState = System.Configuration.ConfigurationManager.AppSettings["Waiting"].ToString();
        private readonly static string ConfirmState = System.Configuration.ConfigurationManager.AppSettings["Confirm"].ToString();
        private readonly static string CancelState = System.Configuration.ConfigurationManager.AppSettings["Cancel"].ToString();
        private readonly static string InsideState = System.Configuration.ConfigurationManager.AppSettings["Inside"].ToString();
        private readonly static string TempConfirmState = System.Configuration.ConfigurationManager.AppSettings["TempConfirm"].ToString();

        public ReservationsController(IReservationRepository resRepository, ICurrencyRepository currencyRepository, IUserActionRepository userActionRepository, IUserRepository userRepository, ICountryRepository countryRepository)
        {
            this._resRepositroy = resRepository;
            this._currencyRepository = currencyRepository;
            this._userActionRepository = userActionRepository;
            this._userRepository = userRepository;
            this._countryRepository = countryRepository;
        }


        // GET: Reservations
        public ActionResult Index()
        {
            ViewBag.Currencies = _currencyRepository.GetAll();
            var actions = _userActionRepository.GetAll();
            ViewBag.Waiting = WaitingState;
            ViewBag.Users = _userRepository.GetAll();
            ViewBag.Confirm1 = TempConfirmState;

            if (UserInfoHelper.UserRole() == "Administrator")
            {
                ViewBag.actions = actions;

                return View(_resRepositroy.GetAll());

            }
            else
            {
                ViewBag.actions = actions.Where(f => f.UserId == UserInfoHelper.GetUserid());
                return View(_resRepositroy.GetAll().Where(i => i.UserId == UserInfoHelper.GetUserid()));

            }
        }

        // GET: Reservations/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.currency = _currencyRepository.GetAll();
            Reservation reservation = _resRepositroy.Get(id);
            if (reservation == null)
            {
                return HttpNotFound();
            }

            if (UserInfoHelper.UserRole() != "Administrator")
            {
                if (UserInfoHelper.GetUserid() != reservation.UserId)
                    return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            ViewBag.DeservedAmount = _userActionRepository.GetAll().FirstOrDefault(f => f.ResId == reservation.Id).DeservedAmount;
            return View(reservation);
        }

        // GET: Reservations/Create
        public ActionResult Create()
        {
            var user = (User)Session["user"];
            var country = _countryRepository.GetByCountryCode(user.CountryCode);
            var currency = _currencyRepository.Get(country.CurrencyId);
            ViewBag.currencyId = currency.Id;
            ViewBag.currencyName = currency.Name;
            return View();
        }

        // POST: Reservations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "VoucherNo,CheckIn,CheckOut,Name,Surname,Birthday,Agenta,ExtraBed,GuestNote,CurrencyId,Person")] Reservation reservation)
        {
            var user = (User)Session["user"];
            long newResId = 0;
            if (ModelState.IsValid)
            {
                try
                {
                    reservation.BookingDate = DateTime.Now;
                    reservation.UserId = UserInfoHelper.GetUserid();
                    newResId = _resRepositroy.Insert(reservation);
                }
                catch (Exception ex)
                {
                    TempData["error"] = "Rezervasyon kaydedilemedi. ";
                    return RedirectToAction("Index");
                }
                try
                {
                    var validCampaign = CampaignsHelper.GetBonusAmount(reservation);

                    decimal FinalBonus = validCampaign.Bonus;
                    if (user.BoundUser != null)
                    {
                        FinalBonus = FinalBonus / 2;
                        var masterAction = new UserAction
                        {
                            BalanceDue = 0,
                            BalanceReceivable = 0,
                            DeservedAmount = FinalBonus,
                            ActionDate = DateTime.Now,
                            ResId = Convert.ToInt32(newResId),
                            UserId = Convert.ToInt32(user.BoundUser),
                            ActionState = WaitingState

                        };
                        _userActionRepository.Insert(masterAction);
                    }

                    var action = new UserAction
                    {
                        BalanceDue = 0,
                        BalanceReceivable = 0,
                        DeservedAmount = FinalBonus,
                        ActionDate = DateTime.Now,
                        ResId = Convert.ToInt32(newResId),
                        UserId = reservation.UserId,
                        ActionState = WaitingState
                    };

                    _userActionRepository.Insert(action);
                }

                catch (Exception ex)
                {
                    TempData["error"] = "Rezervasyon kaydedildi ama kampanya bulunamadı. Lütfen yönetici ile iletişime geçiniz. ";
                    return RedirectToAction("Index");
                }

                return RedirectToAction("Index");
            }
            else
            {
                TempData["error"] = Resources.Resource.zorunluAlan;
            }

            var country = _countryRepository.GetByCountryCode(user.CountryCode);
            var currency = _currencyRepository.Get(country.CurrencyId);
            ViewBag.currencyId = currency.Id;
            ViewBag.currencyName = currency.Name;
            ViewBag.CurrencyList = SelectListItemHelper.CurrencyList(user.CountryCode);
            return View(reservation);
        }

        // GET: Reservations/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reservation reservation = _resRepositroy.Get(id);
            if (reservation == null)
            {
                return HttpNotFound();
            }
            var resConfirme = _userActionRepository.GetAllByResId(id);
            if (resConfirme.Any(f => f.ActionState == ConfirmState) || resConfirme.Any(i => i.ActionState == InsideState))
            {
                TempData["error"] = Resources.Resource.rezEditHataMesaj1;//Confirme veya Inside durumundaki rezervasyonu düzenleyemezsiniz.

                return RedirectToAction("Index");
            }

            if (reservation.UserId != UserInfoHelper.GetUserid() && UserInfoHelper.UserRole() == "Kullanici")
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            var user = (User)Session["user"];
            ViewBag.StateList = SelectListItemHelper.StateKeyValue();
            ViewBag.Currency = _currencyRepository.Get(reservation.CurrencyId).Xml;
            return View(reservation);
        }

        // POST: Reservations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Reservation reservation)//[Bind(Include = "Id,UserId,VoucherNo,CheckIn,CheckOut,Name,Surname,Birthday,Agenta,GuestNote,Amount,CurrencyId")]
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _resRepositroy.Update(reservation);
                    var validCampaign = CampaignsHelper.GetBonusAmount(reservation);

                    var resuser = _userRepository.Get(reservation.UserId);
                    if (resuser.BoundUser != null)
                    {
                        validCampaign.Bonus = validCampaign.Bonus / 2;
                    }

                    var allActions = _userActionRepository.GetAllByResId(reservation.Id);//.FirstOrDefault(f => f.ActionState == WaitingState);
                    foreach (var currentAction in allActions)
                    {
                        if (currentAction.ActionState == TempConfirmState)
                        {
                            currentAction.ActionState = WaitingState;
                        }

                        currentAction.ActionDate = DateTime.Now;
                        currentAction.DeservedAmount = validCampaign.Bonus;
                        _userActionRepository.Update(currentAction);
                    }

                }
                catch (Exception ex)
                {
                    TempData["error"] = "Bir Hata Oluştu. " + ex.Message;
                    return View(reservation);
                }


                return RedirectToAction("Index");
            }

            var user = (User)Session["user"];
            ViewBag.StateList = SelectListItemHelper.StateKeyValue();
            ViewBag.CurrencyList = SelectListItemHelper.CurrencyList(user.CountryCode);
            return View(reservation);
        }

        // GET: Reservations/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reservation reservation = _resRepositroy.Get(id);
            if (reservation == null)
            {
                return HttpNotFound();
            }

            ViewBag.currency = _currencyRepository.Get(reservation.CurrencyId).Xml;
            return View(reservation);
        }

        // POST: Reservations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Reservation reservation = _resRepositroy.Get(id);

            if (reservation.UserId == UserInfoHelper.GetUserid() || UserInfoHelper.UserRole() == "Administrator")
            {
                var userActions = _userActionRepository.GetAllByResId(id);
                if (userActions.Count() == 0)
                {
                    _resRepositroy.Delete(reservation);
                    return RedirectToAction("Index");
                }
                foreach (var action in userActions)
                {
                    if (action.ActionState == WaitingState || action.ActionState == TempConfirmState)
                    {
                        _userActionRepository.Delete(action);
                        _resRepositroy.Delete(reservation);
                        return RedirectToAction("Index");
                    }
                }
                TempData["error"] = "Rezervasyon silinemedi, rezervasyona ait onaylanmış veya iptal edilmiş bir hareket var. ";
                return RedirectToAction("Index");
            }
            return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

        }

    }
}
