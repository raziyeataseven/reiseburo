﻿using ReiseBuro.Entities.Models;
using ReiseBuro.Repository.IRepository;
using ReiseBuro.Web.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ReiseBuro.Web.Controllers
{
    [ReiseAuthorize(Roles = "Administrator")]
    public class ParametersController : Controller
    {
        private readonly IPrmMailRepository _prmMailRepository;
        private readonly ICountryRepository _countryRepository;
        private readonly ICurrencyRepository _currencyRepository;
        private readonly IResStateRepository _resStateRepository;
        private readonly IFloorRepository _floorRepository;
        private readonly ILandscapeRepository _landscapeRepository;
        private readonly ILanguageRepository _langRepository;

        public ParametersController(IPrmMailRepository prmMailRepository, ICountryRepository countryRepository, ICurrencyRepository currencyRepository, IResStateRepository resStateRepository, IFloorRepository floorRepository, ILandscapeRepository landscapeRepository, ILanguageRepository langRepository)
        {
            this._prmMailRepository = prmMailRepository;
            this._countryRepository = countryRepository;
            this._currencyRepository = currencyRepository;
            this._resStateRepository = resStateRepository;
            this._floorRepository = floorRepository;
            this._landscapeRepository = landscapeRepository;
            this._langRepository = langRepository;
        }

        // GET: Parameters

        #region Mails
        public ActionResult Mail()
        {
            return View(_prmMailRepository.GetAll());
        }

        [HttpGet]
        public ActionResult CreateMail()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateMail(Param_Mail mail, HttpPostedFileBase image)
        {
            if (ModelState.IsValid)
            {
                if (image != null)
                {
                    byte[] imgByte = null;
                    imgByte = new byte[image.ContentLength];
                    image.InputStream.Read(imgByte, 0, image.ContentLength);
                    mail.ContactPhoto = Convert.ToBase64String(imgByte);
                }

                _prmMailRepository.Insert(mail);
                return RedirectToAction("Mail");
            }
            return View(mail);
        }

        [HttpGet]
        public ActionResult EditMail(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var mail = _prmMailRepository.Get(id);
            if (mail == null)
            {
                return HttpNotFound();
            }
            ViewBag.Dictionary = Helper.SelectListItemHelper.DictionaryList();
            return View(mail);
        }

        [HttpPost]     
        public ActionResult EditMail(Param_Mail mail, HttpPostedFileBase image)
        {
            if (ModelState.IsValid)
            {

                if (image != null)
                {
                    byte[] imgByte = null;
                    imgByte = new byte[image.ContentLength];
                    image.InputStream.Read(imgByte, 0, image.ContentLength);
                    mail.ContactPhoto = Convert.ToBase64String(imgByte);
                }
                else
                {
                    var tempParam = _prmMailRepository.Get(mail.Id);
                    mail.ContactPhoto = tempParam.ContactPhoto;
                }

                _prmMailRepository.Update(mail);
                return RedirectToAction("Mail");
            }
            ViewBag.Dictionary = Helper.SelectListItemHelper.DictionaryList();
            return View(mail);
        }

        public JsonResult DeleteMail(int id)
        {
            try
            {
                var mail = _prmMailRepository.Get(id);
                _prmMailRepository.Delete(mail);
            }
            catch (Exception ex)
            {
                return Json(new { error = true, message = "İşlem Başarısız. " + ex.Message });
            }

            return Json(new { success = true, message = "İşlem Başarılı" });
        }

        #endregion


        #region countries
        public ActionResult Country()
        {
            ViewBag.Languages = _langRepository.GetAll();
            return View(_countryRepository.GetAll());
        }

        [HttpGet]
        public ActionResult CreateCountry()
        {
            ViewBag.currency = GetCurrencyKeyValue();
            ViewBag.language = Helper.SelectListItemHelper.GetLanguageKeyValue();
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCountry(Country country)
        {
            if (ModelState.IsValid)
            {
                if(_countryRepository.GetAll().Any(f => f.Code == country.Code))
                {
                    TempData["error"] = "Ülke Kodu Daha Önce Tanımlanmış.Litfen Başka Bir Kod Girin.";
                    ViewBag.currency = GetCurrencyKeyValue();
                    return View(country);
                }
                _countryRepository.Insert(country);
                return RedirectToAction("Country");
            }
            ViewBag.currency = GetCurrencyKeyValue();
            return View(country);
        }

        [HttpGet]
        public ActionResult EditCountry(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            ViewBag.language = Helper.SelectListItemHelper.GetLanguageKeyValue();
            ViewBag.currency = GetCurrencyKeyValue();
            return View(_countryRepository.Get(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCountry(Country country)
        {
            if (ModelState.IsValid)
            {
                _countryRepository.Update(country);
                return RedirectToAction("Country");
            }
            ViewBag.language = Helper.SelectListItemHelper.GetLanguageKeyValue();
            ViewBag.currency = GetCurrencyKeyValue();
            return View(country);
        }

        public ActionResult DeleteCountry(int? id)
        {
            if (id != null)
            {
                var obj = _countryRepository.Get(id);
                _countryRepository.Delete(obj);
                return RedirectToAction("Country");
            }
            TempData["error"] = "Bir hata oluştu.";
            return RedirectToAction("Country");
        }
        #endregion


        #region currencies
        public ActionResult Currency()
        {
            return View(_currencyRepository.GetAll());
        }

        [HttpGet]
        public ActionResult CreateCurrency()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCurrency(Currency currency)
        {
            if (ModelState.IsValid)
            {
                _currencyRepository.Insert(currency);
                return RedirectToAction("Currency");
            }

            return View(currency);
        }

        [HttpGet]
        public ActionResult EditCurrency(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            return View(_currencyRepository.Get(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCurrency(Param_Mail mail)
        {
            return View();
        }


        public ActionResult DeleteCurrency(int id)
        {
            return View();
        }
        #endregion

        #region reservation states
        public ActionResult ResState()
        {
            return View(_resStateRepository.GetAll());
        }

        [HttpGet]
        public ActionResult CreateResState()
        {
            ViewBag.Dictionary = ReiseBuro.Web.Helper.SelectListItemHelper.DictionaryList();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateResState(ResState obj)
        {
            if (ModelState.IsValid)
            {
                _resStateRepository.Insert(obj);
                return RedirectToAction("ResState");
            }
            ViewBag.Dictionary = ReiseBuro.Web.Helper.SelectListItemHelper.DictionaryList();
            return View(obj);
        }

        [HttpGet]
        public ActionResult EditResState(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var rezstate = _resStateRepository.Get(id);
            if (rezstate == null)
            {
                return HttpNotFound();
            }
            ViewBag.Dictionary = Helper.SelectListItemHelper.DictionaryList();
            return View(rezstate);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditResState(ResState obj)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Dictionary = Helper.SelectListItemHelper.DictionaryList();
                return View();

            }
            _resStateRepository.Update(obj);
            return RedirectToAction("ResState");

        }

        public ActionResult DeletResState(int? id)
        {
            if (id != null)
            {
                var obj = _countryRepository.Get(id);
                _countryRepository.Delete(obj);
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { error = true }, JsonRequestBehavior.AllowGet);
        }


        #endregion


        #region Floors
        public ActionResult Floor()
        {
            return View(_floorRepository.GetAll().ToList());
        }


        public ActionResult CreateFloor()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateFloor(Floor floor)
        {
            if (ModelState.IsValid)
            {
                _floorRepository.Insert(floor);
                return RedirectToAction("Floor");
            }

            return View(floor);
        }

        [HttpGet]
        public ActionResult EditFloor(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var floor = _floorRepository.Get(id);
            if (floor == null)
            {
                return HttpNotFound();
            }
            return View(floor);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditFloor(Floor floor)
        {
            if (ModelState.IsValid)
            {
                _floorRepository.Update(floor);
                return RedirectToAction("Mail");
            }
            return View(floor);
        }

        public JsonResult DeleteFloor(int id)
        {
            try
            {
                var mail = _prmMailRepository.Get(id);
                _prmMailRepository.Delete(mail);
            }
            catch (Exception ex)
            {
                return Json(new { error = true, message = "İşlem Başarısız. " + ex.Message });
            }

            return Json(new { success = true, message = "İşlem Başarılı" });
        }
        #endregion


        #region Landscape
        public ActionResult Landscape()
        {
            return View(_landscapeRepository.GetAll().ToList());
        }


        public ActionResult CreateLandscape()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateLandscape(Landscape obj)
        {
            if (ModelState.IsValid)
            {
                _landscapeRepository.Insert(obj);
                return RedirectToAction("Floor");
            }

            return View(obj);
        }

        [HttpGet]
        public ActionResult EditLandscape(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var obj = _landscapeRepository.Get(id);
            if (obj == null)
            {
                return HttpNotFound();
            }
            return View(obj);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditLandscape(Landscape obj)
        {
            if (ModelState.IsValid)
            {
                _landscapeRepository.Update(obj);
                return RedirectToAction("Mail");
            }
            return View(obj);
        }

        public JsonResult DeleteLandscape(int id)
        {
            try
            {
                var obj = _landscapeRepository.Get(id);
                _landscapeRepository.Delete(obj);
            }
            catch (Exception ex)
            {
                return Json(new { error = true, message = "İşlem Başarısız. " + ex.Message });
            }

            return Json(new { success = true, message = "İşlem Başarılı" });
        }
        #endregion



        internal List<SelectListItem> GetCurrencyKeyValue()
        {
            var currencies = _currencyRepository.GetAll();
            var list = new List<SelectListItem>();
            foreach (var item in currencies)
            {
                list.Add(new SelectListItem
                {
                    Text = item.Xml,
                    Value = item.Id.ToString()
                });
            }
            return list;

        }

    }
}