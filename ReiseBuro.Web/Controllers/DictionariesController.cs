﻿using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ReiseBuro.Entities.Models;
using ReiseBuro.Web.Models;
using ReiseBuro.Repository.IRepository;
using ReiseBuro.Web.Authentication;

namespace ReiseBuro.Web.Controllers
{
    [ReiseAuthorize(Roles ="Administrator")]
    public class DictionariesController : Controller
    {
        private readonly IDictionaryRepository _dictionaryRepository;

        public DictionariesController(IDictionaryRepository dictionaryRepository)
        {
            this._dictionaryRepository = dictionaryRepository;
        }

        // GET: Dictionaries
        public ActionResult Index()
        {
            return View(_dictionaryRepository.GetAll());
        }

        // GET: Dictionaries/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dictionary dictionary = _dictionaryRepository.Get(id);
            if (dictionary == null)
            {
                return HttpNotFound();
            }
            return View(dictionary);
        }

        // GET: Dictionaries/Create
        public ActionResult Create()
        {
            ViewBag.culture = Helper.SelectListItemHelper.CultureKeyValue();
            return View();
        }

        // POST: Dictionaries/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Dictionary dictionary)
        {
            if (ModelState.IsValid)
            {
                var dic = _dictionaryRepository.GetAll().Where(f => f.D_Key == dictionary.D_Key && f.D_Culture == dictionary.D_Culture);
                if(dic.Count() >0)
                {
                    ViewBag.culture = Helper.SelectListItemHelper.CultureKeyValue();
                    TempData["error"] = "Dil ve anahtar kelime aynı olan başka bir kayıt var, ikinciyi giremezsiniz. Mevcut kaydı düzenleyin veya başka bir anahtar kelime yaratın.";
                    return View(dictionary);
                }
                _dictionaryRepository.Insert(dictionary);
                return RedirectToAction("Index");
            }

            return View(dictionary);
        }

        // GET: Dictionaries/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dictionary dictionary = _dictionaryRepository.Get(id);
            if (dictionary == null)
            {
                return HttpNotFound();
            }
            return View(dictionary);
        }

        // POST: Dictionaries/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Dictionary dictionary)
        {
            if (ModelState.IsValid)
            {
                _dictionaryRepository.Update(dictionary);
                return RedirectToAction("Index");
            }
            return View(dictionary);
        }

        // GET: Dictionaries/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dictionary dictionary = _dictionaryRepository.Get(id);
            if (dictionary == null)
            {
                return HttpNotFound();
            }
            return View(dictionary);
        }

        // POST: Dictionaries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Dictionary dictionary = _dictionaryRepository.Get(id);
            _dictionaryRepository.Delete(dictionary);
            return RedirectToAction("Index");
        }

    }
}
