﻿using System.Linq;
using System.Net;
using System.Web.Mvc;
using ReiseBuro.Entities.Models;
using ReiseBuro.Repository.IRepository;

namespace ReiseBuro.Web.Controllers
{
    public class LanguagesController : Controller
    {
        private readonly ILanguageRepository _langRepsitory;
        private readonly ICountryRepository _countryRepsitory;

        public LanguagesController(ILanguageRepository langRepository, ICountryRepository countryRepsitory)
        {
            this._langRepsitory = langRepository;
            this._countryRepsitory = countryRepsitory;
        }

        // GET: Languages
        public ActionResult Index()
        {
            return View(_langRepsitory.GetAll().ToList());
        }

        // GET: Languages/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Language language = _langRepsitory.Get(id);
            if (language == null)
            {
                return HttpNotFound();
            }
            return View(language);
        }

        // GET: Languages/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Languages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,DisplayName,Culture")] Language language)
        {
            if (ModelState.IsValid)
            {
                _langRepsitory.Insert(language);
                return RedirectToAction("Index");
            }

            return View(language);
        }

        // GET: Languages/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Language language = _langRepsitory.Get(id);
            if (language == null)
            {
                return HttpNotFound();
            }
            return View(language);
        }

        // POST: Languages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,DisplayName,Culture")] Language language)
        {
            if (ModelState.IsValid)
            {
                _langRepsitory.Update(language);
                return RedirectToAction("Index");
            }
            return View(language);
        }

        // GET: Languages/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Language language = _langRepsitory.Get(id);
            if (language == null)
            {
                return HttpNotFound();
            }
            return View(language);
        }

        // POST: Languages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Language language = _langRepsitory.Get(id);
            _langRepsitory.Delete(language);
            return RedirectToAction("Index");
        }


    }
}
