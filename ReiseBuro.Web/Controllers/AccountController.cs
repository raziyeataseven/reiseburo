﻿using ReiseBuro.Entities.Models;
using ReiseBuro.Repository.IRepository;
using ReiseBuro.Web.Helper;
using ReiseBuro.Web.Models;
using System;
using System.Web.Mvc;
using System.Linq;
using ReiseBuro.Web.Authentication;
using System.Net;
using System.Threading;
using System.Globalization;
using ReiseBuro.Repository.Plugins;
using ReiseBuro.Entities.PluginModels;

namespace ReiseBuro.Web.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserRepository _userRepository;
        private readonly ILogTransactionRepository _logRepository;
        private readonly ILanguageRepository _languageRepository;
        private readonly ICountryRepository _countryRepository;
        private readonly IDictionaryRepository _dictionaryRepository;
        private readonly ISmtpMail _mailSender = new SmtpMail();
        private readonly IPrmMailRepository _prmMailRepository;

        public AccountController(IUserRepository userRepository, ILogTransactionRepository logRepository, ILanguageRepository languageRepository, ICountryRepository countryRepository, IDictionaryRepository dictionaryRepository, IPrmMailRepository prmMailRepository)
        {
            _userRepository = userRepository;
            _logRepository = logRepository;
            _languageRepository = languageRepository;
            _countryRepository = countryRepository;
            _dictionaryRepository = dictionaryRepository;
            _prmMailRepository = prmMailRepository;
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult Login(UserViewModel obj)
        {
            if (!ModelState.IsValid)
            {
                return View(obj);
            }

            var user = _userRepository.GetUserLoginObj(obj.UserName, obj.Password);

            if (user == null)
            {
                TempData["invalidLogin"] =Resources.Resource.kullaniciAdiSifreHatali;
                return View(obj);
            }

            if (user.IsActive == false)
            {
                TempData["invalidLogin"] = "Kullanıcı aktif değil vaya yetkilendirme yapılmamış, lütfen yöneticiyle iletişime geçiniz.";
                return View(obj);
            }

            var defaultLang = _languageRepository.Get(_countryRepository.GetByCountryCode(user.CountryCode).LanguageId).Culture;
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(defaultLang);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(defaultLang);
            Session["culture"] = new CultureInfo(defaultLang);

            user.Password = "";
            HttpContext.Session["user"] = user;

            Entities.Helper.SetLanguageVal.AllDictionary = _dictionaryRepository.GetAll();
            Entities.Helper.SetLanguageVal.cultureString = defaultLang;
            return RedirectToAction("Index", "Reservations");
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Register()
        {
            ViewBag.countries = SelectListItemHelper.GetCountryKeyValue();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult Register(RegisterViewModel obj)
        {
            ViewBag.countries = SelectListItemHelper.GetCountryKeyValue();
            if (!ModelState.IsValid)
            {
                return View(obj);
            }
            try
            {
                var existUsers = _userRepository.GetUserbyUsername(obj.Username);
                if (existUsers != null)
                {
                    TempData["error"] = obj.Username + " adına sahip bir hesap zaten var. Lütfen başka bir kullanıcı adı seçin.";
                    return View(obj);
                }
                var userModel = new User
                {
                    Name = obj.Name,
                    Surname = obj.Surname,
                    UserName = obj.Username,
                    UserAddress = obj.UserAddress,
                    City = obj.City,
                    CountryCode = obj.CountryCode,
                    Email = obj.Email,
                    Password = obj.Password,
                    Gsm = obj.Gsm,
                    Telephone = obj.Telephone,
                    PersonInCharge = obj.PersonInCharge,
                    RegistryDate = DateTime.Now,
                    IsActive = false,
                    Role = "Kullanici"
                };

                _userRepository.Insert(userModel);
                TempData["message"] = "Kullanıcı Başarıyla Kaydoldu. Hesabınız aktif edildikten sonra giriş yapabilirsiniz.";
                return RedirectToAction("Login");
            }
            catch (Exception ex)
            {
                TempData["message"] = "Bir Hata Oluştu. " + ex.Message;
                return View();
            }

        }

        [ReiseAuthorize(Roles = "Administrator,Kullanici")]
        public ActionResult LogOut()
        {
            if (HttpContext.Session["Login"] == null)
            {
                return RedirectToAction("Login");
            }

            var log = new Log_Transaction { UserId = UserInfoHelper.GetUserid(), TransDate = DateTime.Now, TransComment = "User Logout." };
            _logRepository.Insert(log);

            HttpContext.Session["user"] = null;
            return RedirectToAction("Login");
        }

        [HttpGet]
        [ReiseAuthorize(Roles = "Administrator,Kullanici")]
        public ActionResult Profil()
        {
            ViewBag.Countries = Helper.SelectListItemHelper.GetCountryKeyValue();
            return View(_userRepository.Get(UserInfoHelper.GetUserid()));
        }

        [HttpPost]
        [ReiseAuthorize(Roles = "Administrator,Kullanici")]
        [ValidateAntiForgeryToken]
        public ActionResult Profil(User user)
        {
            if (!ModelState.IsValid)
                return View(user);

            if (UserInfoHelper.GetUserid() != user.Id || UserInfoHelper.UserRole() != "Administrator")
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            try
            {
                var tempUser = _userRepository.Get(user.Id);
                tempUser.Name = user.Name;
                tempUser.Surname = user.Surname;
                tempUser.UserName = user.UserName;
                tempUser.UserAddress = user.UserAddress;
                tempUser.Telephone = user.Telephone;
                tempUser.Gsm = user.Gsm;
                tempUser.Email = user.Email;
                tempUser.CountryCode = user.CountryCode;
                tempUser.City = user.City;

                _userRepository.Update(tempUser);
                TempData["message"] = "İşlem Başarılı";
            }
            catch (Exception ex)
            {
                TempData["message"] = "Bir Hata Oluştu.";
            }

            ViewBag.Countries = SelectListItemHelper.GetCountryKeyValue();
            return View();
        }


        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult ForgotPassword(string Email)
        {
            try
            {
                var user = _userRepository.GetUserbyUsername(Email);
                if (user == null)
                {
                    TempData["error"] = Resources.Resource.parolamiUnuttumError;                  
                }
                else
                {
                    var mailConf = _prmMailRepository.GetActiveMail();
                    var smtpParam = new SmtpParam
                    {
                        DisplayMail = mailConf.DisplayMail,
                        MailAdres = mailConf.MailAddress,
                        Host = mailConf.Host,
                        Port = mailConf.Port,
                        Ssl = mailConf.Ssl,
                        Password = mailConf.Parola,
                        DisplayName = mailConf.DisplayMail
                    };

                    var mailObj = new MailObject
                    {
                        CC_List = mailConf.CcList,
                        HtmlContent = Resources.Resource.kullanici + " : " + user.UserName + ", " + Resources.Resource.parola + " : " + user.Password,
                        MailAdres=user.Email,
                        Subject=Resources.Resource.parolamiunuttum
                    };

                    _mailSender.SendMail(mailObj, smtpParam);
                    TempData["message"] = Resources.Resource.mailGonderildi;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return View();
        }
    }
}