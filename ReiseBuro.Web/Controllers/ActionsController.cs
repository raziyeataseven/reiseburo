﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ReiseBuro.Entities.Models;
using ReiseBuro.Web.Models;
using ReiseBuro.Repository.IRepository;
using ReiseBuro.Web.Authentication;
using ReiseBuro.Web.Helper;

namespace ReiseBuro.Web.Controllers
{
    [ReiseAuthorize(Roles = "Administrator")]
    public class ActionsController : Controller
    {
        private readonly IUserActionRepository _userActionRepository;
        public ActionsController(IUserActionRepository userActionRepository)

        {
            this._userActionRepository = userActionRepository;
        }

        [ReiseAuthorize(Roles ="Administrator")]
        // GET: Actions
        public ActionResult Index()
        {
            return View(_userActionRepository.GetAll().ToList());
        }

        // GET: Actions/Details/5
        [ReiseAuthorize(Roles = "Administrator")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserAction userAction = _userActionRepository.Get(id);
            if (userAction == null)
            {
                return HttpNotFound();
            }
            return View(userAction);
        }

        // GET: Actions/Create
        [ReiseAuthorize(Roles = "Administrator")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Actions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ReiseAuthorize(Roles = "Administrator")]
        public ActionResult Create([Bind(Include = "Id,ActionDate,ResId,BalanceDue,BalanceReceivable,ActionState")] UserAction userAction)
        {
            if (ModelState.IsValid)
            {
               _userActionRepository.Insert(userAction);       
                return RedirectToAction("Index");
            }

            return View(userAction);
        }

        // GET: Actions/Edit/5
        [ReiseAuthorize(Roles = "Administrator")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserAction userAction = _userActionRepository.Get(id);
            if (userAction == null)
            {
                return HttpNotFound();
            }
            return View(userAction);
        }

        // POST: Actions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ReiseAuthorize(Roles = "Administrator")]
        public ActionResult Edit([Bind(Include = "Id,ActionDate,ResId,BalanceDue,BalanceReceivable,ActionState")] UserAction userAction)
        {
            if (ModelState.IsValid)
            {
                _userActionRepository.Update(userAction);
                return RedirectToAction("Index");
            }
            return View(userAction);
        }

        // GET: Actions/Delete/5
        [ReiseAuthorize(Roles = "Administrator")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserAction userAction = _userActionRepository.Get(id);
            if (userAction == null)
            {
                return HttpNotFound();
            }
            return View(userAction);
        }

        // POST: Actions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [ReiseAuthorize(Roles = "Administrator")]
        public ActionResult DeleteConfirmed(int id)
        {
            UserAction userAction = _userActionRepository.Get(id);          
            _userActionRepository.Delete(userAction);
            return RedirectToAction("Index");
        }

    
    }
}
