﻿using ReiseBuro.Entities.Models;
using ReiseBuro.Entities.PluginModels;
using ReiseBuro.Repository.IRepository;
using ReiseBuro.Repository.Plugins;
using ReiseBuro.Web.Authentication;
using ReiseBuro.Web.Helper;
using ReiseBuro.Web.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace ReiseBuro.Web.Controllers
{
    [ReiseAuthorize(Roles = "Administrator,Kullanici")]
    public class HomeController : Controller
    {
        private readonly ICampaignRepository _campaignRepository;
        private readonly IPrmMailRepository _prmMailRepository;
        private readonly ISmtpMail _mailSender = new SmtpMail();
        private readonly IUserRepository _userRepository;
        private readonly IImageRepository _imageRepository;
        private readonly ILanguageRepository _langRepository;

        public HomeController(ICampaignRepository campaignRepository, IPrmMailRepository prmMailRepository, IUserRepository userRepository, IImageRepository imageRepository, ILanguageRepository langRepository)
        {
            this._campaignRepository = campaignRepository;
            this._prmMailRepository = prmMailRepository;
            this._userRepository = userRepository;
            this._imageRepository = imageRepository;
            this._langRepository = langRepository;
        }

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Contact(int? id)
        {
            if (id != null)
            {
                TempData["user"] = _userRepository.Get(id);
            }

            ViewBag.Mail = _prmMailRepository.GetActiveMail();
            return View();
        }


        [HttpPost]
        public ActionResult Contact(MailViewModel mail)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var user = (User)Session["user"];
                    var mailConf = _prmMailRepository.GetAll().FirstOrDefault(f => f.IsActive == true);

                    var mailObject = new MailObject
                    {
                        HtmlContent = mail.Content,
                        Subject = mail.Subject ?? "",
                        CC_List = mailConf.CcList
                    };
                    var displaySenderMail = "";
                    if (String.IsNullOrEmpty(mail.MailAddress))
                    {
                        displaySenderMail = user.Email;
                        mailObject.HtmlContent += " Mail Address: " + displaySenderMail;
                        mailObject.MailAdres = mailConf.MailAddress;
                    }
                    else
                    {
                        displaySenderMail = mail.MailAddress;
                        mailObject.MailAdres = displaySenderMail;
                    }

                    var smtpParam = new SmtpParam
                    {
                        DisplayMail = mailConf.DisplayMail,
                        MailAdres = mailConf.MailAddress,
                        Host = mailConf.Host,
                        Port = mailConf.Port,
                        Ssl = mailConf.Ssl,
                        Password = mailConf.Parola,
                        DisplayName = mailConf.DisplayMail
                    };

                    _mailSender.SendMail(mailObject, smtpParam);
                    TempData["message"] = Resources.Resource.mailGonderildi;
                    return RedirectToAction("Contact");
                }
                catch (Exception ex)
                {
                    ViewBag.Mail = _prmMailRepository.GetActiveMail();
                    TempData["message"] = "Error. " + ex.Message;
                    return View(mail);
                }

            }
            ViewBag.Mail = _prmMailRepository.GetActiveMail();
            TempData["message"] = Resources.Resource.zorunluAlan;
            return View();
        }


        public ActionResult Info()
        {
            ViewBag.languages = _langRepository.GetAll();
            var ImageModel = _imageRepository.GetAll().OrderBy(f => f.ImageOrder).ToList();
            return View(ImageModel);
        }

        public ActionResult Images()
        {
            ViewBag.languages = _langRepository.GetAll();
            var ImageModel = _imageRepository.GetAll().OrderBy(f => f.ImageOrder).ToList();
            return View(ImageModel);
        }


        public ActionResult EditPhoto(int? id)
        {
            ViewBag.lang = SelectListItemHelper.CultureKeyValue();
            var model = _imageRepository.Get(id);
            var imageViewModel = new ImageViewModel
            {
                Culture = model.Culture,
                Description = model.ImageDescription,
                ImageOrder = model.ImageOrder,
                ImagePath = model.ImagePath,
                Caption = model.ImageCaption,
                Id = model.Id
            };

            return View(imageViewModel);
        }

        [HttpPost]
        public ActionResult EditPhoto(ImageViewModel obj)
        {
            if (ModelState.IsValid)
            {
                var image = new Image
                {
                    ImageDescription = obj.Description,
                    Id = obj.Id,
                    ImageOrder = obj.ImageOrder
                };

                if (obj.ImageData != null)
                {
                    var file = obj.ImageData;
                    var allowedExtensions = new[] {
            ".Jpg", ".png", ".jpg", "jpeg"
                            };

                    image.ImageCaption = file.FileName;
                    var fileName = Path.GetFileName(file.FileName);
                    var ext = Path.GetExtension(file.FileName);
                    if (allowedExtensions.Contains(ext))
                    {
                        string name = Path.GetFileNameWithoutExtension(fileName);
                        string myfile = obj.ImageOrder + "_" + name + "_" + ext;
                        var path = Path.Combine(Server.MapPath("~/Uploads"), myfile);
                        image.ImagePath = path;                       
                        file.SaveAs(path);
                    }
                }
                else
                {
                    image.ImageCaption = obj.Caption;
                    image.ImagePath = obj.ImagePath;
                }

                _imageRepository.Update(image);
                return RedirectToAction("Images");
            }

            ViewBag.lang = Helper.SelectListItemHelper.GetLanguageKeyValue();
            return View(obj);
        }



        [HttpGet]
        public ActionResult AddPhoto()
        {
            ViewBag.langs = ReiseBuro.Web.Helper.SelectListItemHelper.CultureKeyValue();
            return View();
        }

        [HttpPost]
        public ActionResult AddPhoto(ImageViewModel imageViewModel)
        {
            if (ModelState.IsValid)
            {
                var image = new Image
                {
                    ImageOrder = imageViewModel.ImageOrder,
                    Culture = imageViewModel.Culture,
                    ImageDescription = imageViewModel.Description
                };

                var file = imageViewModel.ImageData;
                var allowedExtensions = new[] {
            ".Jpg", ".png", ".jpg", "jpeg"
        };

                image.ImageCaption = file.FileName;
                var fileName = Path.GetFileName(file.FileName);
                var ext = Path.GetExtension(file.FileName);
                if (allowedExtensions.Contains(ext))
                {
                    string name = Path.GetFileNameWithoutExtension(fileName);
                    string myfile = imageViewModel.ImageOrder + "_" + name + "_" + ext;
                    var path = Path.Combine(Server.MapPath("~/Uploads"), myfile);
                    image.ImagePath = path;
                    _imageRepository.Insert(image);
                    file.SaveAs(path);
                    return RedirectToAction("AddPhoto");
                }
                else
                {
                    ViewBag.message = "Lütfen sadece resim dosyası ekleyin.";
                }
            }

            ViewBag.culture = SelectListItemHelper.CultureKeyValue();
            return View(imageViewModel);
        }


        public ActionResult DeletePhoto(int? id)
        {
            return View();
        }


        [AllowAnonymous]
        [HttpPost]
        public ActionResult SelectLanguage(string data)
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(data);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(data);
            Session["culture"] = new CultureInfo(data);
            Entities.Helper.SetLanguageVal.cultureString = data;
            return Json(true);
        }
    }
}