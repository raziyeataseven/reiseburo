﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ReiseBuro.Entities.Models;
using ReiseBuro.Web.Models;
using ReiseBuro.Repository.IRepository;
using ReiseBuro.Web.Authentication;
using ReiseBuro.Web.Helper;
using System.Configuration;

namespace ReiseBuro.Web.Controllers
{

    public class UserActionsController : Controller
    {
        private readonly IUserActionRepository _userActionRepository;
        private readonly IResStateRepository _resStateRepository;
        private readonly IUserRepository _userRepository;
        private readonly IReservationRepository _resRepository;

        private readonly string CancelStatus = ConfigurationManager.AppSettings["Cancel"].ToString();
        private readonly string ConfirmStatus = ConfigurationManager.AppSettings["Confirm"].ToString();
        private readonly string InsideStatus = ConfigurationManager.AppSettings["Inside"].ToString();
        private readonly string FirstConfirm = ConfigurationManager.AppSettings["TempConfirm"].ToString();
        private readonly string PaymentStatus = ConfigurationManager.AppSettings["Payment"].ToString();

        public UserActionsController(IUserActionRepository userActionRepository, IResStateRepository resStateRepository, IUserRepository userRepository, IReservationRepository resRepository)
        {
            this._userActionRepository = userActionRepository;
            this._resStateRepository = resStateRepository;
            this._userRepository = userRepository;
            this._resRepository = resRepository;
        }

        // GET: UserActions
        [ReiseAuthorize(Roles = "Administrator")]
        public ActionResult Index()
        {
            var userActions = _userActionRepository.GetAll();
            ViewBag.Res = _resRepository.GetAll();
            ViewBag.states = SelectListItemHelper.GetDisplayResStatus();/*_resStateRepository.GetAll();*/
            ViewBag.user = _userRepository.GetAll();
            return View(userActions.ToList());
        }

        [ReiseAuthorize(Roles = "Kullanici")]
        public ActionResult Actions()
        {
            ViewBag.users = _userRepository.GetAll();
            ViewBag.states = SelectListItemHelper.GetDisplayResStatus();
            //ViewBag.states = _resStateRepository.GetAll();
            ViewBag.reservations = _resRepository.GetAll();
            return View(_userActionRepository.GetAll().Where(f => f.UserId == UserInfoHelper.GetUserid()));
        }

        [ReiseAuthorize(Roles = "Administrator")]
        public ActionResult UserAction(int id)  
        {
           ViewBag.Username = _userRepository.Get(id).UserName;
            ViewBag.states = SelectListItemHelper.GetDisplayResStatus();
            return View(_userActionRepository.GetAll().Where(f => f.UserId == id));
        }

        // GET: UserActions/Details/5
        [ReiseAuthorize(Roles = "Administrator")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserAction userAction = _userActionRepository.Get(id);
            if (userAction == null)
            {
                return HttpNotFound();
            }
            return View(userAction);
        }


        [ReiseAuthorize(Roles = "Administrator")]
        public ActionResult Create(int? id)
        {
            var user = _userRepository.Get(id);
            if(user==null)
            {
                return HttpNotFound();
            }
            user.Password = "";
            ViewBag.user = user;         
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ReiseAuthorize(Roles = "Administrator")]
        public ActionResult Create(UserAction userAction)
        {
            if (ModelState.IsValid)
            {
                userAction.ActionDate = DateTime.Now;
                if (userAction.ResId == null)
                {
                    userAction.ActionState = PaymentStatus;
                }
                _userActionRepository.Insert(userAction);
                TempData["message"] = "İşlem Başarılı.";
                return RedirectToAction("UserAction", new { id=userAction.UserId });
            }
            ViewBag.user = _userRepository.Get(userAction.UserId);
            return View(userAction);
        }

        // GET: UserActions/Edit/5
        [ReiseAuthorize(Roles = "Administrator")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserAction userAction = _userActionRepository.Get(id);
            if (userAction == null)
            {
                return HttpNotFound();
            }
            ViewBag.Dictionary = SelectListItemHelper.DictionaryList();
            ViewBag.Users = SelectListItemHelper.GetUsersKeyValue();
            return View(userAction);
        }

        [ReiseAuthorize(Roles = "Administrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserAction userAction)
        {
            if (ModelState.IsValid)
            {
                _userActionRepository.Update(userAction);
                return RedirectToAction("Index");
            }
            ViewBag.Dictionary = SelectListItemHelper.DictionaryList();
            ViewBag.Users = SelectListItemHelper.GetUsersKeyValue();
            return View(userAction);
        }

        // GET: UserActions/Delete/5
        [ReiseAuthorize(Roles = "Administrator")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserAction userAction = _userActionRepository.Get(id);
            if (userAction == null)
            {
                return HttpNotFound();
            }
            ViewBag.Username = _userRepository.Get(userAction.UserId).UserName;
            //ViewBag.Voucher = _resRepository.Get(userAction.ResId).VoucherNo;
            return View(userAction);
        }

        // POST: UserActions/Delete/5
        [ReiseAuthorize(Roles = "Administrator")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UserAction userAction = _userActionRepository.Get(id);
            _userActionRepository.Delete(userAction);
            TempData["message"] = "Hareket başarıyla silindi.";
            return RedirectToAction("UserAction", new { id = userAction.UserId });
        }

        [ReiseAuthorize(Roles = "Administrator")]
        public ActionResult Cancel(int? id)
        {
            if (id == null)
                TempData["error"] = "İşlem gerçekleştirilemiyor.";

            try
            {
                var action = _userActionRepository.Get(id);
                action.BalanceReceivable = decimal.Zero;
                action.BalanceDue = decimal.Zero;

                action.ActionState = CancelStatus;
                _userActionRepository.Update(action);

                var res = _resRepository.Get(action.ResId);
                res.Amount = decimal.Zero;
                _resRepository.Update(res);

                TempData["message"] = "İşlem Başarılı";
            }
            catch (Exception ex)
            {
                TempData["error"] = "Bir Hata Oluştu. " + ex.Message;
            }

            return RedirectToAction("Index");

        }


        [ReiseAuthorize(Roles = "Administrator")]
        public ActionResult Confirm(int? id)
        {
            if (id == null)
            {
                TempData["error"] = "İşlem gerçekleştirilemiyor.";
                return RedirectToAction("Index");
            }

            try
            {
                var action = _userActionRepository.Get(id);
                if (action.DeservedAmount == decimal.Zero)
                {
                    TempData["error"] = "Hakedilen miktar sıfırdan büyük olmalıdır.";
                    return RedirectToAction("Index");
                }
                action.ActionState = ConfirmStatus;
                action.BalanceReceivable = action.DeservedAmount;
                _userActionRepository.Update(action);

                var res = _resRepository.Get(action.ResId);
                res.Amount = action.DeservedAmount;
                _resRepository.Update(res);

                TempData["message"] = "İşlem Başarılı";
            }
            catch (Exception ex)
            {
                TempData["error"] = "Bir Hata Oluştu. " + ex.Message;
            }

            return RedirectToAction("Index");

        }

        [ReiseAuthorize(Roles ="Administrator")]
        public ActionResult Inside(int? id)
        {
            if (id == null)
            {
                TempData["error"] = "İşlem gerçekleştirilemiyor.";
                return RedirectToAction("Index");
            }
            try
            {
                var action = _userActionRepository.Get(id);
                action.ActionState = InsideStatus;
                _userActionRepository.Update(action);
                TempData["message"] = "İşlem Başarılı";

            }
            catch (Exception ex)
            {
                TempData["error"] = "Bir Hata Oluştu. " + ex.Message;
            }

            return RedirectToAction("Index");
        }


        [ReiseAuthorize(Roles ="Administrator")]
        public ActionResult TempConfirm(int? id)
        {         
            try
            {
                var action = _userActionRepository.Get(id);
                action.ActionState = FirstConfirm;
                _userActionRepository.Update(action);
                TempData["message"] = "İşlem Başarılı. ";
            }
            catch (Exception ex)
            {
                TempData["error"] = "Bir Hata Oluştu. " + ex.Message;
            }
            return RedirectToAction("Index");
        }


        public ActionResult Summary()
        {
            var userActions = _userActionRepository.GetAll();
            ViewBag.user = _userRepository.GetAll();
            return View(userActions.ToList());
        }

    }
}
