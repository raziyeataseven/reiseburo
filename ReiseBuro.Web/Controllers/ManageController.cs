﻿using ReiseBuro.Entities.Models;
using ReiseBuro.Repository.IRepository;
using ReiseBuro.Web.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ReiseBuro.Web.Controllers
{
    [ReiseAuthorize(Roles = "Administrator")]
    public class ManageController : Controller
    {

        private readonly IUserRepository _userRepository;
        private readonly ICountryRepository _countryRepository;

        public ManageController(IUserRepository userRepository, ICountryRepository countryRepository)
        {
            this._userRepository = userRepository;
            this._countryRepository = countryRepository;
        }
        // GET: Manage
        public ActionResult Index()
        {
            ViewBag.country = _countryRepository.GetAll();
            var list = _userRepository.GetAll().OrderBy(i => i.Id).ToList();
            ViewBag.bounduser = _userRepository.GetAll();
            return View(list);
        }

        [HttpGet]
        public JsonResult ConfirmUser(int[] id)
        {
            try
            {
                foreach (var item in id)
                {
                    var user = _userRepository.Get(item);
                    user.IsActive = true;
                    _userRepository.Update(user);
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { error = true, message = "Bir hata oluştu. " + ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult Delete(int? id)
        {
            try
            {
                var user = _userRepository.Get(id);
                _userRepository.Delete(user);
                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                return Json(new { error = true, message = "Bir Hata Oluştu. " + ex.Message });
            }

        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            var temp= Helper.SelectListItemHelper.GetCountryKeyValue();
            ViewBag.countries = temp;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var user = _userRepository.Get(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            temp.Where(f => f.Value == user.CountryCode).Where(i => i.Selected = true);
            ViewBag.countries = temp;
            var usersList = Helper.SelectListItemHelper.GetUsersKeyValue();
            if (user.BoundUser != null)
            {
                usersList.FirstOrDefault(f => f.Value == user.BoundUser.ToString()).Selected = true;
            }
            ViewBag.users = usersList;
            ViewBag.roles = Helper.SelectListItemHelper.GetRoles();
            return View(user);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Surname,UserName,Password,IsActive,Role,PersonInCharge,UserAddress,Country,City,RegistryDate,ConfirmDate,Email,Gsm,Telephone,UserId,BoundUser")]User user)
        {
            ViewBag.roles = Helper.SelectListItemHelper.GetRoles();
            if (!ModelState.IsValid)
            {
                return View(user);
            }

            if (user.Email != user.UserName)
            {
                TempData["error"] = "Kullanıcı adı ve email aynı olmalı";
                return View(user);
            }

            if (user.CountryCode == null)
            {
                var tempUser = _userRepository.Get(user.Id);
                user.CountryCode = tempUser.CountryCode;
            }
            var temp = _userRepository.Get(user.Id);
            if (temp.IsActive == false && user.IsActive == true)
                user.ConfirmDate = DateTime.Now;

            _userRepository.Update(user);         
            return RedirectToAction("Index");
        }


    }
}