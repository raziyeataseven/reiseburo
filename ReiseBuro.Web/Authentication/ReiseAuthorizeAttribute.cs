﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using ReiseBuro.Web.Models;
using ReiseBuro.Entities.Models;

namespace ReiseBuro.Web.Authentication
{
    public class ReiseAuthorizeAttribute : AuthorizeAttribute
    {

        private readonly string _redirectUrl = "/Account/Login";
        private string[] allowedroles;


        protected void CustomAuthorizeAttribute(params string[] roles)
        {
            this.allowedroles = roles;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (HttpContext.Current.Session?["user"] == null)
                return false;

            allowedroles = this.Roles.Split(',');

            var user = (User)HttpContext.Current.Session["user"];
            bool authorize = false;
            foreach (var role in allowedroles)
            {
                if (user.IsActive==true && allowedroles.Contains(user.Role))
                {
                    authorize = true; 
                }
            }
            return authorize;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectResult(_redirectUrl);
        }
    }
}


