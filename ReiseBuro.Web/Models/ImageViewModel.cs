﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ReiseBuro.Web.Models
{
    public class ImageViewModel
    {
        public int Id { get; set; }
        public string ImagePath { get; set; }
        public string Culture { get; set; }
        public string Caption { get; set; }
        public string Description { get; set; }
        public int? ImageOrder { get; set; }
        
        public HttpPostedFileBase ImageData { get; set; }

    }
}