﻿using ReiseBuro.Web.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ReiseBuro.Web.Models
{
    public class RegisterViewModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
            ErrorMessageResourceName = "zorunluAlanlariDoldurunuz")]
        [Display(Name ="Adı")]
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
           ErrorMessageResourceName = "zorunluAlanlariDoldurunuz")]
        [Display(Name ="Soyadı")]
        public string Surname { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
           ErrorMessageResourceName = "zorunluAlanlariDoldurunuz")]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
          ErrorMessageResourceName = "zorunluAlanlariDoldurunuz")]        
        [Display(Name = "Email Tekrar")]
        [Compare("Email", ErrorMessage ="Girilen Email adresleri uyuşmuyor.")]
        public string Username { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
            ErrorMessageResourceName = "zorunluAlanlariDoldurunuz")]
        [StringLength(100, ErrorMessage = "{0} en az {2} karakter uzunluğunda olmalıdır.", MinimumLength = 3)]
        [Display(Name = "Şifre")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
           ErrorMessageResourceName = "zorunluAlanlariDoldurunuz")]
        [Display(Name = "Şifre Tekrar")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Şifreler Uyuşmuyor.")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Yetkili Kişi")]
        public string PersonInCharge { get; set; }

        [Display(Name = "Adres")]
        public string UserAddress { get; set; }

        [Display(Name ="Ülke")]
        public string CountryCode { get; set; }

        [Display(Name ="Şehir")]
        public string City { get; set; }

        [Display(Name ="Cep Tel.")]
        [DataType(DataType.PhoneNumber)]
        public string Gsm { get; set; }

        [Display(Name ="İş Tel.")]
        [DataType(DataType.PhoneNumber)]
        public string Telephone { get; set; }
    }
}