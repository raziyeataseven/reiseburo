﻿using ReiseBuro.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReiseBuro.Web.Models
{
    public class Param_MailViewModel
    {
        public Param_Mail Param_Mail { get; set; }
        public HttpPostedFileBase ContactPicture { get; set; }

    }
}