﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ReiseBuro.Web.Models
{
    public class ReiseBuroWebContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public ReiseBuroWebContext() : base("name=ReiseBuroWebContext")
        {
        }

        public System.Data.Entity.DbSet<ReiseBuro.Entities.Models.Reservation> Reservations { get; set; }

        public System.Data.Entity.DbSet<ReiseBuro.Entities.Models.User> Users { get; set; }

        public System.Data.Entity.DbSet<ReiseBuro.Entities.Models.Campaign> Campaigns { get; set; }

        public System.Data.Entity.DbSet<ReiseBuro.Entities.Models.Param_Mail> Param_Mail { get; set; }

        public System.Data.Entity.DbSet<ReiseBuro.Entities.Models.UserAction> UserActions { get; set; }

        public System.Data.Entity.DbSet<ReiseBuro.Entities.Models.Country> Countries { get; set; }

        public System.Data.Entity.DbSet<ReiseBuro.Entities.Models.Currency> Currencies { get; set; }

        public System.Data.Entity.DbSet<ReiseBuro.Entities.Models.ResState> ResStates { get; set; }

        public System.Data.Entity.DbSet<ReiseBuro.Entities.Models.Landscape> Landscapes { get; set; }

        public System.Data.Entity.DbSet<ReiseBuro.Entities.Models.Floor> Floors { get; set; }

        public System.Data.Entity.DbSet<ReiseBuro.Entities.Models.Dictionary> Dictionaries { get; set; }

        public System.Data.Entity.DbSet<ReiseBuro.Entities.Models.Language> Languages { get; set; }
    }
}
