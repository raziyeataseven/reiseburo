﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReiseBuro.Web.Models
{
    public class CampaignOzetModel
    {
        public DateTime BasTarih { get; set; }
        public DateTime BitTarih { get; set; }
        public decimal Miktar { get; set; }
    }
}