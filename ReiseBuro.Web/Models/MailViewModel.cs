﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ReiseBuro.Web.Models
{
    public class MailViewModel
    {
        [Required]
        public string Content { get; set; }

        public string Subject { get; set; }
        public string MailAddress { get; set; }
    }
}