﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReiseBuro.Entities;
namespace ReiseBuro.Web.Models
{
    public class ResStatusViewModel
    {
        public int Id { get; set; }
       
        public string ConfirmState { get; set; }
       
        public string ConfirmCode { get; set; }

        public string ActionKeyword { get; set; }

        public string DisplayName { get { return ReiseBuro.Entities.Helper.SetLanguageVal.GetContent(this.ActionKeyword); } set { _displayName = value; } }

        private string _displayName;

        public ResStatusViewModel()
        {
            if (this.ActionKeyword == null)
                DisplayName = ConfirmState;

        }
    }
}