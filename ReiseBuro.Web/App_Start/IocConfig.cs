﻿using ReiseBuro.Repository.Repository;
using System;
using System.Reflection;
using Autofac;
using Autofac.Integration.Mvc;
using System.Web.Mvc;

namespace ReiseBuro.Web
{
    public class IocConfig
    {
        public static void Configure()
        {
            var containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterControllers(Assembly.GetExecutingAssembly());
            containerBuilder.RegisterAssemblyTypes(typeof(CampaignRepository).Assembly)
                            .Where(s => s.Name.EndsWith("Repository"))
                            .AsImplementedInterfaces()
                            .InstancePerRequest();

            var container = containerBuilder.Build();
            var resolver = new AutofacDependencyResolver(container);
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}